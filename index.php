<?php
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/protected/vendor/autoload.php';
require __DIR__ . '/protected/vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/protected/config/web.php';

/*tideways_xhprof_enable();*/

(new yii\web\Application($config))->run();

/*$data = tideways_xhprof_disable();
file_put_contents(
    sys_get_temp_dir() . "/" . uniqid() . ".yourapp.xhprof",
    serialize($data)
);*/
