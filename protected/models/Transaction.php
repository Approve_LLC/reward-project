<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string $amount
 * @property string $info
 * @property string $created
 * @property string $changed
 */
class Transaction extends ActiveRecord
{
    const TYPE_PROMOCODE_REGISTER_BONUS = 'promocode_register_bonus'; // За рег. с промо-кодом
    const TYPE_REFERRAL_REWARD = 'referral_reward';   // Доход от реферала

    private $_user;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            [['user_id'], 'integer'],
            [['type', 'info'], 'string'],
            ['type', 'default', 'value'=>null],
            ['type', 'in', 'range'=>array_keys(static::getTypesLabels())],
            [['amount'], 'number'],
            [['user_id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id', 'message' => 'Пользователь не найден.'],
        ];
    }

    public static function getTypesLabels()
    {
        return [
            '' => 'Не задано',
            static::TYPE_PROMOCODE_REGISTER_BONUS => 'Бонус за регистрацию с промо-кодом',
            static::TYPE_REFERRAL_REWARD => 'Доход от реферала',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'type' => 'Тип',
            'amount' => 'Сумма',
            'info' => 'Info',
            'created' => 'Создана',
            'changed' => 'Изменена',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // update user balance
        if ($insert)
            $this->getUser()->balance += $this->amount;
        elseif (@$changedAttributes['amount'])
            $this->getUser()->balance += $this->amount - @$changedAttributes['amount']; // changedAttributes - старые значения

        if (!$this->getUser()->save())
            throw new \yii\base\Exception('Error updating user balance.');
    }

    public function afterDelete()
    {
        parent::afterDelete();

        // update user balance
        $this->getUser()->balance -= $this->amount;
        $this->getUser()->save();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user_id = $user->id;
        $this->_user = $user;
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->_user)
            return $this->_user;

        return $this->_user = User::find()->where(['id'=>$this->user_id])->one();
    }
}
