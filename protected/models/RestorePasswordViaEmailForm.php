<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RestorePasswordViaEmailForm
 * @package app\modules\v2\models
 */
class RestorePasswordViaEmailForm extends Model
{
    public $email;
    public $app_id;

    /**
     * @var bool|User
     */
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'app_id'], 'required'],
            ['app_id', 'integer'],
            ['email', 'email'],
            ['email', 'validateUser'],
        ];
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Аккаунт не найден.');
                return;
            }

            if ($user->status != User::STATUS_ACTIVE) {
                $this->addError($attribute, 'Аккаунт не активен.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_id' => 'app_id',
            'email' => 'email',
        ];
    }


    /**
     * Finds user
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByLoginAndAppId($this->email, $this->app_id);
        }

        return $this->_user;
    }
}
