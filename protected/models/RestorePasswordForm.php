<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RestorePasswordForm
 * @package app\modules\v2\models
 */
class RestorePasswordForm extends Model
{
    public $app_id;
    public $login;
    public $code;
    public $new_password;

    /**
     * @var bool|User
     */
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login','code','new_password','app_id'], 'required'],
            [['new_password'], 'string', 'min' => 1, 'max' => 32],
            ['app_id', 'integer'],
            ['login', 'validateUserAndCode'],
        ];
    }

    public function validateUserAndCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user) {
                $this->addError($attribute, 'Аккаунт не найден.');
                return;
            }

            if ($user->status != User::STATUS_ACTIVE) {
                $this->addError($attribute, 'Аккаунт не активен.');
            }

            if (!$user->password_restore_code) {
                $this->addError('code', 'Код не верен.');
            }

            if ($user->password_restore_code != $this->code) {
                $this->addError('code', 'Код не верен.');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_id' => 'app_id',
            'login' => 'login',
            'code' => 'code',
            'new_password' => 'new_password',
        ];
    }


    /**
     * Finds user
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByLoginAndAppId($this->login, $this->app_id);
        }

        return $this->_user;
    }

    /**
     * @return bool
     */
    public function saveNewPassword()
    {
        $user = $this->getUser();
        $user->password_restore_code = null;
        $user->password = $this->new_password;
        return $user->save();
    }
}
