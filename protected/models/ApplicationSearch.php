<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Application;

/**
 * ApplicationSearch represents the model behind the search form of `app\models\Application`.
 */
class ApplicationSearch extends Application
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tasks_to_earn_referer_reward'], 'integer'],
            [['name', 'secret', 'adcolony_key', 'created', 'changed', 'task_fields_set', 'version'], 'safe'],
            [['register_with_promocode_reward', 'first_referer_reward', 'next_referer_rewards', 'version'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Application::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'register_with_promocode_reward' => $this->register_with_promocode_reward,
            'tasks_to_earn_referer_reward' => $this->tasks_to_earn_referer_reward,
            'first_referer_reward' => $this->first_referer_reward,
            'next_referer_rewards' => $this->next_referer_rewards,
            'version' => $this->version,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'secret', $this->secret])
            ->andFilterWhere(['like', 'adcolony_key', $this->adcolony_key])
            ->andFilterWhere(['like', 'task_fields_set', $this->task_fields_set])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'changed', $this->changed]);

        return $dataProvider;
    }
}
