<?php

namespace app\models;

use app\components\RewardActiveRecord;

/**
 * Класс - приложение, взаимодействующее с бэкендом.
 *
 * @property int $id
 * @property string $name
 * @property string $secret
 * @property string $adcolony_key
 * @property string $created
 * @property string $changed
 * @property float $register_with_promocode_reward  награда за регистрацию с промокодом
 * @property int $tasks_to_earn_referer_reward      кол-во заданий за вып. которых владелец промо-кода получает награду
 * @property float $first_referer_reward            первая награда
 * @property float $next_referer_rewards            последующая
 * @property string $task_fields_set                название набора дополнительных полей для заданий, для хранения в TaskType::$custom_data
 */
class Application extends RewardActiveRecord
{
    const TASK_FIELDS_SET_NONE = '';
    const TASK_FIELDS_SET_DETAILED_TASKS = 'detailed-tasks';
    const TASK_FIELDS_SET_IMG_STRING = 'img-string';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'version'], 'required'],
            [['name'], 'string', 'min' => 1, 'max' => 64],
            ['task_fields_set', 'in', 'range' => array_keys(static::getTaskFieldsSetsLabels())],
            [['adcolony_key'], 'string', 'max' => 128],
            [['secret'], 'string', 'max' => 32],
            [['update_message'], 'string'],
            [['force'], 'boolean'],
            // for referral system
            [['register_with_promocode_reward', 'first_referer_reward', 'next_referer_rewards'], 'default', 'value' => 0],
            [['register_with_promocode_reward', 'first_referer_reward', 'next_referer_rewards'], 'filter', 'filter' => 'strval'],
            [['register_with_promocode_reward', 'first_referer_reward', 'next_referer_rewards'], 'string', 'max' => 15], // дробное число из 15 символов включая точку
            [['register_with_promocode_reward', 'first_referer_reward', 'next_referer_rewards'], 'number'],
            ['tasks_to_earn_referer_reward', 'default', 'value' => 1],
            ['tasks_to_earn_referer_reward', 'integer', 'min' => 1],
            ['version', 'string', 'min' => 1, 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'secret' => 'Секрет',
            'adcolony_key' => 'Ключ с AdColony.com',
            'created' => 'Создано',
            'changed' => 'Изменено',
            'register_with_promocode_reward' => 'Награда за регистрацию с промокодом',
            'tasks_to_earn_referer_reward' => 'Кол-во (N) заданий вып. рефералом, за которое владелец промо-кода получает награду',
            'first_referer_reward' => 'Награда за первые N заданий',
            'next_referer_rewards' => 'Награда за каждые последующие N заданий',
            'task_fields_set' => 'Набор дополнительных полей для заданий',
            'version' => 'Версия',
            'force' => 'Критичность обновления',
            'update_message' => 'Update message',
        ];
    }

    public static function getTaskFieldsSetsLabels()
    {
        return [
            static::TASK_FIELDS_SET_NONE => 'Одно поле',
            static::TASK_FIELDS_SET_DETAILED_TASKS => 'Заголовок, подзаголовок, описание, картинка, текст кнопки, bundle id',
            static::TASK_FIELDS_SET_IMG_STRING => 'Картинка и ссылка',
        ];
    }

    public function fields()
    {
        return [
            'latestVersion' => function ($model) {
                return $model->version;
            },
            'forceUpgrade' => function ($model) {
                return $model->force;
            },
            'updateMessage' => function ($model) {
                return $model->update_message;
            }
        ];
    }

    /**
     * Выдает набор дополнительный полей для заданий этого приложения.
     * @return array массив элементов: [name => [label=>'Заголовок',input=>'textInput']]
     */
    public function getTaskFieldsSet()
    {
        $fieldsSets = [
            static::TASK_FIELDS_SET_NONE => [
                'data' => [
                    'label' => 'Произвольные данные',
                    'input' => 'textarea',
                ],
            ],
            static::TASK_FIELDS_SET_DETAILED_TASKS => [
                'name' => [
                    'label' => 'Заголовок',
                    'input' => 'textInput',
                ],
                'name2' => [
                    'label' => 'Подзаголовок',
                    'input' => 'textInput',
                ],
                'description' => [
                    'label' => 'Описание',
                    'input' => 'textarea',
                ],
                'image' => [
                    'label' => 'Картинка',
                    'input' => 'textInput',
                ],
                'button' => [
                    'label' => 'Текст кнопки',
                    'input' => 'textInput',
                ],
                'bundleId' => [
                    'label' => 'Bundle ID',
                    'input' => 'textInput',
                ],
            ],
            static::TASK_FIELDS_SET_IMG_STRING => [
                'image' => [
                    'label' => 'Картинка',
                    'input' => 'textInput',
                ],
                'link' => [
                    'label' => 'Ссылка',
                    'input' => 'textInput',
                ],
            ],
        ];

        return @$fieldsSets[$this->task_fields_set];
    }

    public static function getDropdownArr()
    {
        $data = Application::find()->select(['id', 'name'])->asArray()->all();
        $arr = [];
        array_map(function ($value) use (&$arr, $data) {
            $arr[$value['id']] = $value['name'];
        }, $data);
        return $arr;
    }
}
