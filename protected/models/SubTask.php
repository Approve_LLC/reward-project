<?php

namespace app\models;

use paulzi\jsonBehavior\JsonValidator;
use Yii;

use paulzi\jsonBehavior\JsonBehavior;

/**
 * This is the model class for table "sub_task".
 *
 * @property int $id
 * @property int $task_id
 * @property int $parent_id
 * @property int $custom_data
 * @property int $amount
 * @property int $type
 * @property int $interval
 * @property string $extra_field
 *
 * @property TaskType $task
 */
class SubTask extends \yii\db\ActiveRecord
{

    const INTERVAL_DIMENSION_SECONDS = 'seconds';
    const INTERVAL_DIMENSION_MINUTES = 'minutes';
    const INTERVAL_DIMENSION_HOURS = 'hours';
    const INTERVAL_DIMENSION_DAYS = 'days';

    const SCENARIO_UPDATE = 'update';

    const TASK_SPENT_TIME = 0;
    const TASK_COUNT_ENTRIES = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id'], 'required'],
            [['task_id', 'parent_id', 'custom_data', 'type', 'interval'], 'integer'],
            ['amount', 'filter', 'filter' => 'strval'],
            ['amount', 'string', 'max' => 15], // дробное число из 15 символов включая точку
            ['amount', 'number'],
            [['extra_field'], JsonValidator::class],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaskType::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'custom_data' => Yii::t('app', 'Данные'),
            'amount' => Yii::t('app', 'Вознаграждение'),
            'type' => Yii::t('app', 'Тип задачи'),
            'interval' => Yii::t('app', 'Интервал')
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => JsonBehavior::className(),
                'attributes' => ['extra_field'],
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE] = ['id', 'task_id', 'parent_id', 'custom_data', 'amount', 'type', 'interval', 'extra_field'];
        return $scenarios;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->interval = $this->getInterval();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_id']);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->scenario != self::SCENARIO_UPDATE) {
                $this->parent_id = $id = SubTask::find()
                    ->select('id')
                    ->where(['task_id' => $this->task_id])
                    ->max('id');
            }
            return true;
        } else {
            return false;
        }
    }

    public function setInterval($dimension)
    {
        switch ($dimension) {
            case self::INTERVAL_DIMENSION_MINUTES:
                $this->interval = $this->interval * 60;
                break;
            case self::INTERVAL_DIMENSION_HOURS:
                $this->interval = $this->interval * 60 * 60;
                break;
            case self::INTERVAL_DIMENSION_DAYS:
                $this->interval = $this->interval * 60 * 60 * 24;
                break;
        }
    }

    public function getInterval()
    {
        $result = $this->interval;
        switch ($this->extra_field['time_dimension']) {
            case self::INTERVAL_DIMENSION_MINUTES:
                $result = $this->interval / 60;
                break;
            case self::INTERVAL_DIMENSION_HOURS:
                $result = $this->interval / (60 * 60);
                break;
            case self::INTERVAL_DIMENSION_DAYS:
                $result = $this->interval / (60 * 60 * 24);
                break;
        }
        return $result;
    }

    /**
     * @return array
     */
    public static function getIntervalDimensions()
    {
        return [
            self::INTERVAL_DIMENSION_SECONDS => 'Секунды',
            self::INTERVAL_DIMENSION_MINUTES => 'Минуты',
            self::INTERVAL_DIMENSION_HOURS => 'Часы',
            self::INTERVAL_DIMENSION_DAYS => 'Дни'
        ];
    }

    /**
     * @param User $user
     * @param $taskId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSubTasksForUserByTaskId(User $user, $taskId)
    {
        $subTasks = static::findBySql('
             SELECT DISTINCT s.id AS subtask_id,
               s.task_id,
               s.custom_data,
               s.amount,
               s.type,
               s.interval, 
               s.extra_field,               
               (SELECT COUNT(*) as cnt FROM `report` WHERE subtask_id = s.id) as closed
             FROM `sub_task` s
             /*чей родитель сделан или тот, у кого нет родителя*/
             JOIN `report` r ON (s.parent_id IS NULL OR s.parent_id = r.subtask_id) 
             /*проверяем, что у сделанного задания закончился интервал времени*/
              AND r.created < (NOW() - INTERVAL s.interval SECOND)
              AND r.user_id = :user_id 
              AND s.task_id = r.task_type_id
             WHERE s.task_id = :task_id
             ORDER BY subtask_id ASC')->params([
            ':user_id' => $user->id,
            ':task_id' => $taskId
        ])->asArray()->all();

        return $subTasks;
    }
}
