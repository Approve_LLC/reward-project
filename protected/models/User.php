<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property int $application_id
 * @property int $type
 * @property int $status
 * @property string $login
 * @property string $password_hash
 * @property string $promo_code        по-умолчанию = null и промо-кодом служит $id
 * @property string $promoCode         это getter и setter
 * @property int    $referrer_user_id  кто пригласил
 * @property string $access_token
 * @property string $password_restore_code
 * @property string $auth_key
 * @property int    $balance
 * @property string $created
 *
 * virtual:
 * @property string $password пароль для хеширования в $password_hash
 * @property string $referer_promocode промо-код юзера, который пригласил.
 * @property integer $count_referrers количество рефереров.
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $password;
    public $referer_promocode;
    public $count_referrers;
    public $sum_rewards_for_referrers;

    private $_application;

    const TYPE_ADMIN = 0;
    const TYPE_MANAGER = 1;
    const TYPE_USER = 3;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCKED = -1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public static function generatePasswordRestoreCode()
    {
        return mt_rand(100000, 999999);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'type', 'status'], 'integer'],
            ['login', 'trim'],
            ['application_id', 'filter', 'filter' => function($value){
                if (!$value)
                    return null;
                return $value;
            }],
            ['password', 'hashPassword'],
            ['login', 'required'],
            ['password', 'required', 'enableClientValidation' => false, 'when' => function($model) {
                return !$model->password_hash;
            }],
            [['password'], 'string', 'min' => 1, 'max' => 32],
            [['login', 'password_hash'], 'string', 'max' => 128],
            [['login'], 'string', 'min' => 1],
            ['status', 'in', 'range' => array_keys(User::getStatusLabels())],
            ['type',   'in', 'range' => array_keys(User::getTypesLabels())],
            ['!balance', 'filter', 'filter' => 'floatval'],
            ['!balance', 'filter', 'filter' => 'strval'],
            ['!balance', 'string', 'max' => 15], // дробное число из 15 символов включая точку
            ['!balance', 'number'],
            ['!balance', 'default', 'value' => 0, 'when'=> function ($model) {
                /* @var $model User */
                return $model->type == static::TYPE_USER;
            }],
            ['login', 'unique', 'targetAttribute' => ['login','application_id'], 'message' => 'Такой логин уже занят.'],
            [['access_token'], 'unique'],
            ['application_id', 'exist', 'targetClass' => Application::className(), 'targetAttribute' => 'id', 'message' => 'Приложение с указанным ID не найдено.'],
            ['promo_code', 'filter', 'filter' => 'trim'],
            ['promo_code', 'default', 'value' => null],
            ['promo_code', 'validatePromoCode'],
            ['referer_promocode', function($attribute){
                // проверить существует ли промо-код и записать ID владельца промо-кода
                if ($user = static::findByPromoCode($this->$attribute))
                    $this->referrer_user_id = $user->id;
                else
                    $this->addError($attribute, 'Промокод не найден.');
            }],
        ];
    }

    public function validatePromoCode($attribute, $params)
    {
        if (preg_match('/^\d+$/', $this->$attribute)) {
            $this->addError($attribute, 'Промо-код не должен состоять только из цифр.');
            return;
        }

        $exist = (new Query())->select('id')
            ->from(static::tableName())
            ->where([
                'application_id' => $this->application_id,
                'promo_code' => $this->$attribute,
            ])
            ->andWhere(new Expression('id != '.intval($this->id)))
            ->scalar();

        if ($exist) {
            $this->addError($attribute, 'Такой промо-код уже занят.');
            return;
        }
    }

    public function hashPassword($attribute, $params)
    {
        if (!$this->$attribute)
            return;

        $this->password_hash = Yii::$app->security->generatePasswordHash($this->$attribute);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'ID приложения',
            'type' => 'Тип',
            'status' => 'Статус',
            'login' => 'Логин',
            'password' => 'Пароль',
            'password_hash' => 'Password Hash',
            'password_restore_code' => 'Код восстановления пароля',
            'auth_key' => 'Auth Key',
            'balance' => 'Баланс',
            'promo_code' => 'Промо-код',
            'promoCode' => 'Промо-код',
            'referrer_user_id' => 'ID пригл. пользователя',
            'count_referrers' => 'Сколько пригласил',
            'sum_rewards_for_referrers' => 'Доход от приглашенных',
            'created' => 'Дата создания',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            // reward for promocode
            if ($this->referer_promocode && $this->getApplication()->register_with_promocode_reward) {
                $transaction = new Transaction;
                $transaction->amount = $this->getApplication()->register_with_promocode_reward;
                $transaction->user_id = $this->id;
                if (!$transaction->save())
                    throw new \yii\db\Exception('Reward for promocode transaction not saved!');
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public static function getTypesLabels()
    {
        return [
            static::TYPE_ADMIN => 'Администратор',
            static::TYPE_USER => 'Пользователь',
            static::TYPE_MANAGER => 'Менеджер',
        ];
    }

    public static function getStatusLabels() {
        return [
            static::STATUS_ACTIVE => 'Активен',
//            static::STATUS_INACTIVE => 'Не активен',
            static::STATUS_BLOCKED => 'Заблокирован',
        ];
    }

    /**
     * @param int|string $id
     * @return null|User
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Генерирует, сохраняет и выдает token.
     *
     * @param int $length max 32
     * @return string access_token
     */
    public function makeAccessToken($length = 32)
    {
        $this->access_token = substr(md5(microtime()), 0, $length);
        $this->save(false, ['access_token']);
        return $this->access_token;
    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return User
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    public static function findByLoginAndAppId($login, $application_id = null)
    {
        return static::findOne(['login' => $login, 'application_id' => $application_id]);
    }

    /**
     * @param string|int $promoCode
     * @return null|User
     */
    public static function findByPromoCode($promoCode)
    {
        if (preg_match('/^\d+$/', $promoCode))
            return static::findOne(['id' => $promoCode]);

        return static::findOne(['promo_code' => $promoCode]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getUsername()
    {
        return $this->login;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        if (!$password)
            return;

        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    /**
     * @return null|Application приложение к которому принадлежит пользователь.
     */
    public function getApplication()
    {
        if ($this->_application) {
            return $this->_application;
        }

        return $this->_application = Application::find()
            ->where(['id'=>$this->application_id])->one();
    }

    /**
     * Высылает код. Можно использовать только если login это email
     * @return bool
     */
    public function sendRestorePasswordEmail()
    {
        $this->password_restore_code = static::generatePasswordRestoreCode();

        return $this->save() && Yii::$app->mailer
                ->compose('passwordRestoreCode', ['code' => $this->password_restore_code])
                ->setFrom('noreply@' . Yii::$app->request->serverName)
                ->setTo($this->login)
                ->setSubject('Восстановление пароля')
                ->send();
    }

    public function setPromoCode($value = null)
    {
        $this->promo_code = $value;
    }

    public function getPromoCode()
    {
        return $this->promo_code ?: $this->id;
    }

    public function countReports()
    {
        return (new Query())->select('count(*)')
            ->from(Report::tableName())
            ->where(['user_id' => $this->id])
            ->scalar();
    }

    public function getReferrers()
    {
        return $this->hasMany(static::className(), ['id'=>'referrer_user_id']);
    }

    /**
     * Выдает кол-во
     * @return int
     */
    public function countReferrers()
    {
        return (integer) (new Query())->select('count(*)')
            ->from(static::tableName())
            ->where(['referrer_user_id' => $this->id])
            ->scalar();
    }

    /**
     * @return float
     */
    public function getSumRewardsForReferrers()
    {
        return (float) (new Query())->select('sum(amount)')
            ->from(Transaction::tableName())
            ->where([
                'user_id' => $this->id,
                'type' => Transaction::TYPE_REFERRAL_REWARD,
            ])
            ->scalar();
    }

    /**
     * Пересчитывает балансы пользователей (на основе выплат и отчетов).
     * @return int num updated
     * @throws \yii\db\Exception
     */
    static public function recalculateBalances()
    {
        $command = (new Query())->createCommand();
        $command->sql = '
            UPDATE user u LEFT JOIN 
    
            (SELECT user_id, SUM(`amount`) AS amount FROM
            (
                SELECT user_id, SUM(`amount`) AS amount
                FROM `report`
                GROUP BY user_id
                
                UNION
                
                SELECT user_id, SUM(`amount`) AS amount
                FROM `payment`
                GROUP BY user_id
                
                UNION
                
                SELECT user_id, SUM(`amount`) AS amount
                FROM `transaction`
                GROUP BY user_id
            ) amounts_tables
            GROUP BY user_id) a 
            
            ON (u.id = a.user_id)
            
            SET u.balance = a.amount
        ';

        return $command->execute();
    }
}
