<?php

namespace app\models;

use dosamigos\taggable\Taggable;
use Yii;
use yii\helpers\Json;

use app\components\RewardActiveRecord;

/**
 * This is the model class for table "task_type".
 *
 * @property int $id
 * @property int $application_id
 * @property int $min_period
 * @property int $amount
 * @property string $name
 * @property string $custom_data
 * @property string $created
 * @property string $changed
 * @property int $limit
 * @property int $limitCount
 */
class TaskType extends RewardActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['application_id', 'min_period', 'amount', 'name', 'limit'], 'required'],
            [['application_id', 'min_period'], 'integer', 'min' => 0],
            ['application_id', 'exist', 'targetClass' => Application::className(), 'targetAttribute' => 'id', 'message' => 'Приложение не найдено.'],
            ['amount', 'filter', 'filter' => 'strval'],
            ['amount', 'string', 'max' => 15], // дробное число из 15 символов включая точку
            ['amount', 'number'],
            ['name', 'string', 'min' => 1, 'max' => 64],
            ['custom_data', 'filter', 'filter' => function ($v) {
                if (is_array($v))
                    return Json::encode($v);
                else
                    return $v;
            }],
            ['custom_data', 'default', 'value' => null],
            [['tagNames'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'ID приложения',
            'min_period' => 'Мин. период',
            'amount' => 'Вознаграждение',
            'name' => 'Название',
            'custom_data' => 'Произвольные данные',
            'created' => 'Создано',
            'changed' => 'Изменено',
            'limit' => 'Лимит заданий в день',
            'limitCount' => 'Текущая цифра счетчика',
        ];
    }

    public function behaviors()
    {

        $array = array_merge(parent::behaviors(), [
            'taggable' => [
                'class' => Taggable::className(),
                'relation' => 'extraFields'
            ],
        ]);
        return $array;
    }

    /**
     * Выдает типы заданий для юзера, со временем в секундах, через которое задание можно выполнить.
     * @param User $user
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\db\Exception
     */
    public static function getListForUser(User $user)
    {
        $setGroupBy = Yii::$app->db
            ->createCommand("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))")
            ->execute();

        $taskTypes = static::findBySql('SELECT t.id,
       t.name,
       t.amount                                                                                         AS reward,
       IF(NOW() - max(rep.created) - t.min_period < 0, ABS(NOW() - max(rep.created) - t.min_period), 0) AS wait,
       t.custom_data,
       (COUNT(DISTINCT sub.id))                                                                         as subtasks_total,
       (SELECT COUNT(DISTINCT subtask_id) as cnt
        FROM `report`
        WHERE `report`.subtask_id = sub.id
          AND `report`.task_type_id = sub.task_id)                                                      as closed_subtasks,
       IF(COUNT(DISTINCT sub.id) != 0, TRUE, FALSE)                                                     as hasSubtasks,
       IF((COUNT(DISTINCT rep.id) != 0), 1, 0)                                                   as taskDone
FROM `task_type` AS t
       LEFT JOIN `report` rep ON (t.id = rep.task_type_id) AND (rep.user_id = :user_id)
       LEFT JOIN `sub_task` sub ON (t.id = sub.task_id)
WHERE t.application_id = :application_id
  AND ((t.limitCount < t.limit) OR t.limit = 0)
GROUP BY t.id
HAVING IF(subtasks_total = closed_subtasks AND taskDone = 1, 0, 1)'
        )->params([
            ':user_id' => $user->id,
            ':application_id' => $user->application_id
        ])->asArray()->all();

        $taskTypes = array_map(function ($taskTypes) {
            $taskTypes['hasSubtasks'] = !empty($taskTypes['hasSubtasks']);
            $taskTypes['taskDone'] = !empty($taskTypes['taskDone']);
            return $taskTypes;
        }, $taskTypes);

        return $taskTypes;
    }

    /**
     * @return array
     */
    public function getCustomFieldSet()
    {
        $application = Application::findOne($this->application_id);

        if ($application)
            return $application->getTaskFieldsSet();
        else
            return [];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getCustomTaskTypeFields()
    {
        $fields = $this->getExtraFields()->asArray()->all();
        return $fields;
    }

    /**
     * @param string $fieldName
     * @return string
     */
    public function getCustomFieldValue($fieldName)
    {
        $customData = Json::decode($this->custom_data);
        return @strval($customData[$fieldName]);  // вдруг кто-то массив послал
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(TaskType::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getExtraFields()
    {
        return $this->hasMany(ExtraField::className(), ['id' => 'extra_field_id'])
            ->viaTable('task_type_extra_field', ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubTasks()
    {
        return $this->hasMany(SubTask::className(), ['task_id' => 'id']);
    }

    /**
     * @param $taskTypeId
     */
    public static function incrementLimit($taskTypeId)
    {
        $task = self::findOne(['id' => $taskTypeId]);
        if ($task->limitCount < $task->limit) {
            $task->limitCount++;
            $task->save();
        }
    }

    /**
     * @param $taskTypeId
     */
    public static function decrementLimit($taskTypeId, $count = 1)
    {
        $task = self::findOne(['id' => $taskTypeId]);
        if ($task->limitCount > 0) {
            $task->limitCount -= $count;
            $task->save();
        }
    }

    /**
     * @param $taskTypeId
     * @return bool
     */
    public static function checkLimit($taskTypeId)
    {
        $res = false;
        $task = self::findOne(['id' => $taskTypeId]);
        $res = ($task->limitCount < $task->limit) || ($task->limit == 0);
        return $res;
    }
}
