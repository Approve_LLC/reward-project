<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class ApiLoginForm
 * @package app\modules\v2\models
 */
class ApiLoginForm extends Model
{
    public $login;
    public $password;
    public $app_id;
    public $rememberMe = true;

    public $status = 1; // API status, 0 - ok, not 0 - not ok

    /**
     * @var bool|User
     */
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['login', 'password', 'app_id'], 'required'],
            ['app_id', 'integer'],
            ['rememberMe', 'boolean'],
            ['login', 'validateUser'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rememberMe' => 'rememberMe',
            'app_id' => 'app_id',
            'login' => 'login',
            'password' => 'password',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неправильные login или пароль.');
            }
        }
    }

    public function validateUser($attribute, $params)
    {
        if ($this->hasErrors())
            return;

        $user = $this->getUser();

        if (!$user) {
            $this->addError($attribute, 'Аккаунт не найден.');
            $this->status = 3;
            return;
        }

        if ($user->status != User::STATUS_ACTIVE) {
            $this->addError($attribute, 'Аккаунт не активен.');
            $this->status = 2;
        }
    }

    /**
     * Logs in a user using the provided login and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        return false;
    }

    /**
     * Finds user
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByLoginAndAppId($this->login, $this->app_id);
        }

        return $this->_user;
    }
}
