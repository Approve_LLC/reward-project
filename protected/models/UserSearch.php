<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_id','referrer_user_id',
                'type', 'status', 'balance', 'count_referrers', 'sum_rewards_for_referrers'], 'integer'],
            [['login', 'access_token', 'auth_key', 'created'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->select('u.id,u.application_id,u.login,u.type,u.status,u.balance, count(0) AS count_referrers,
                count(0) AS sum_rewards_for_referrers')
            ->alias('u')
            //->leftJoin(static::tableName().' r', 'u.id = r.referrer_user_id')
            //->leftJoin(Transaction::tableName().' t', 'u.id = t.user_id AND t.type = :type')
            //->addParams([':type'=>Transaction::TYPE_REFERRAL_REWARD])
            ->groupBy('u.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	    'pagination' => [
            	'pageSize' => 20,
    	    ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'u.id' => $this->id,
            'u.application_id' => $this->application_id,
  //          'u.referrer_user_id' => $this->referrer_user_id,
            'u.type' => $this->type,
            'u.status' => $this->status,
            'u.balance' => $this->balance,
//            'u.created' => $this->created,
        ]);
        $query->andFilterWhere(['like', 'u.login', $this->login]);
        //$query->andFilterHaving(['count_referrers' => $this->count_referrers]);
        //$query->andFilterHaving(['sum_rewards_for_referrers' => $this->sum_rewards_for_referrers]);


        return $dataProvider;
    }
}
