<?php

namespace app\models;

use Yii;
use yii\db\Query;
use app\components\RewardActiveRecord;

/**
 * Отчет о выполнении задания.
 * This is the model class for table "report".
 *
 * @property int $id
 * @property int $user_id
 * @property int $task_type_id
 * @property int $subtask_id
 * @property int $amount
 * @property string $date_start
 * @property string $date_end
 * @property string $unique_id
 * @property string $info
 * @property string $created
 * @property string $changed
 *
 * virtual:
 * @property string $hash
 * @property User $user user associated with this Report
 * @property TaskType $task_type read-only
 */
class Report extends RewardActiveRecord
{
    public $hash;

    private $_user;
    private $_task_type;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount'], 'required'],
            ['user_id', 'filter', 'filter' => function ($value) {
                return preg_replace('/[^\d]/', '', $value);
            }],
            [['user_id', 'task_type_id', 'subtask_id'], 'integer'],
            ['amount', 'filter', 'filter' => 'strval'],
            ['amount', 'string', 'max' => 15], // дробное число из 15 символов включая точку
            ['amount', 'number'],
            [['date_start', 'date_end'], 'safe'],
            [['user_id'], 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id', 'message' => 'Пользователь не найден.'],
            [['task_type_id'], 'exist', 'targetClass' => TaskType::className(), 'targetAttribute' => 'id', 'message' => 'Тип задания не найден.', 'when' => function (Report $model) {
                return $model->task_type_id > 0;
            }],
            [['!unique_id'], 'unique', 'isEmpty' => function ($value) {
                return is_null($value);
            }],
            [['info'], 'string'],

            // for api:
            [['!user_id', '!amount', 'hash'], 'required', 'on' => 'api'],
            [['hash'], 'validateHash', 'on' => 'api'],
            [['task_type_id'], 'validateTimeLimit', 'on' => 'api'],

            [['date_start', 'date_end'], 'filter', 'filter' => function ($value) {
                // normalize date-time format
                if (preg_match('/^\d+$/', $value))
                    return explode('+', date('c', $value))[0];
                else
                    return $value;
            }],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateHash($attribute, $params)
    {
        /* @var User $user */
        $user = $this->getUser();
        $secret = $user->getApplication()->secret;

        if ($this->$attribute != md5($this->task_type_id . $this->date_start . $this->date_end . $secret))
            $this->addError($attribute, 'hash не верен.');
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateTimeLimit($attribute, $params)
    {
        if (empty($this->getTaskType()->min_period)) {
            return;
        }
        $secondsFromLastReport = $this->getSecondsFromLastReport();

        if ($secondsFromLastReport
            && $secondsFromLastReport <= $this->getTaskType()->min_period
        ) {
            $secondsToWait = $this->getTaskType()->min_period - $secondsFromLastReport;
            $this->addError($attribute, 'Прошло слишком мало времени после последнего отчета об этом задании. Осталось секунд: ' . $secondsToWait);
        }
    }

    public function getSecondsFromLastReport()
    {
        return (new Query)->select('NOW() - max(created)')
            ->from(Report::tableName())
            ->where([
                'user_id' => $this->user_id,
                'task_type_id' => $this->task_type_id,
            ])->scalar();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'task_type_id' => 'ID типа задания',
            'subtask_id' => 'ID подзадания',
            'amount' => 'Вознаграждение',
            'date_start' => 'Время начала',
            'date_end' => 'Время конца',
            'unique_id' => 'Доп. ID',
            'info' => 'Инфо',
            'created' => 'Создан',
            'changed' => 'Изменен',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // update user balance
        if ($insert)
            $this->getUser()->balance += $this->amount;
        elseif (@$changedAttributes['amount'])
            $this->getUser()->balance += $this->amount - @$changedAttributes['amount']; // changedAttributes - старые значения

        if (!$this->getUser()->save())
            throw new \yii\base\Exception('Error updating user balance.');

        // награда за реферальство, если есть
        $app = $this->getUser()->getApplication();
        if ($app && $app->tasks_to_earn_referer_reward && ($app->first_referer_reward || $app->next_referer_rewards)) {
            $countReports = $this->getUser()->countReports();
            // если делится без остатка
            if (!($countReports % $app->tasks_to_earn_referer_reward)) {
                if ($countReports == $app->tasks_to_earn_referer_reward)
                    $reward = $app->first_referer_reward;
                else
                    $reward = $app->next_referer_rewards;

                $refId = $this->getUser()->referrer_user_id;
                if (!empty($refId)) {
                    $transaction = new Transaction;
                    $transaction->amount = $reward;
                    $transaction->type = Transaction::TYPE_REFERRAL_REWARD;
                    $transaction->user_id = $refId;
                    if (!$transaction->save()) {
                        throw new \yii\db\Exception($transaction->user_id);
                    }
                }
            }
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        // update user balance
        $this->getUser()->balance -= $this->amount;
        $this->getUser()->save();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user_id = $user->id;
        $this->_user = $user;
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->_user)
            return $this->_user;

        return $this->_user = User::find()->where(['id' => $this->user_id])->one();
    }

    /**
     * @return null|TaskType
     */
    public function getTaskType()
    {
        if ($this->_task_type) {
            return $this->_task_type;
        }

        if ($this->task_type_id) {
            return $this->_task_type = TaskType::find()->where(['id' => $this->task_type_id])->one();
        } else {
            return null;
        }
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getSubtask()
    {
        $this->getTaskType();
        if ($this->task_type_id && $this->subtask_id) {
            return SubTask::find()
                ->where(['id' => $this->subtask_id])
                ->andWhere(['task_id' => $this->task_type_id])->one();

        } else {
            return null;
        }
    }

    /**
     * @return null
     */
    public function setAmountFromSubtask()
    {
        if ($this->getSubtask()) {
            return $this->amount = $this->getSubtask()->amount;
        } else {
            return null;
        }
    }

    /**
     * Устанавливает вознаграждение из задания, если ID задания указан.
     */
    public function setAmountFromTaskType()
    {
        if ($this->getTaskType()) {
            $this->amount = $this->getTaskType()->amount;
        } else {
            return null;
        }
    }
}
