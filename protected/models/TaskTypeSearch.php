<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TaskTypeSearch represents the model behind the search form of `app\models\TaskType`.
 */
class TaskTypeSearch extends TaskType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'application_id', 'min_period', 'limit', 'limitCount'], 'integer'],
            [['amount'], 'number'],
            [['name', 'custom_data', 'created', 'changed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TaskType::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'application_id' => $this->application_id,
            'min_period' => $this->min_period,
            'amount' => $this->amount,
            'limit' => $this->limit,
            'limitCount' => $this->limitCount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'custom_data', $this->custom_data])
            ->andFilterWhere(['like', 'created', $this->created])
            ->andFilterWhere(['like', 'changed', $this->changed]);

        return $dataProvider;
    }
}
