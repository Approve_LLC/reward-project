<?php

namespace app\models;

use app\components\RewardActiveRecord;
use yii\db\Query;

/**
 * Выплата
 * This is the model class for table "payment".
 *
 * @property int $id
 * @property int $user_id
 * @property int $application_id
 * @property int $amount
 * @property string $status
 * @property string $payment_type
 * @property string $payment_account
 * @property string $note
 * @property string $created
 * @property string $changed
 */
class Payment extends RewardActiveRecord
{
    private $_user;

    const STATUS_WAITING   = 'waiting';
    const STATUS_EXECUTING = 'executing';
    const STATUS_DONE      = 'done';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'payment_type', 'payment_account'], 'required'],
            [['user_id'], 'integer'],
            [['status'], 'in', 'range' => [static::STATUS_WAITING,static::STATUS_EXECUTING,static::STATUS_DONE]],
            [['payment_type', 'payment_account'], 'string', 'min' => 2, 'max' => 64],
            [['note'], 'string', 'max' => 250],
            ['amount', 'filter', 'filter' => 'strval'],
            ['amount', 'string', 'max' => 15], // дробное число из 15 символов включая точку
            ['amount', 'number', 'min' => 0.0000000000001, 'message' => 'Сумма должна быть не меньше 0.0000000000001'],
            ['amount', 'validateUserBalance'],
            // for api:
            [['!user_id','!note','!status'], 'safe', 'on' => 'api'],
            [['amount', 'payment_type', 'payment_account'], 'safe', 'on' => 'api'],
        ];
    }

    public function validateUserBalance($attribute, $params)
    {
        if (!$this->getUser()) {
            $this->addError('user_id', 'Пользователь не найден.');
            return;
        }

        if ($this->$attribute > $this->getUser()->balance)
            $this->addError($attribute, 'Недостаточно средств на балансе.');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        if ($this->scenario == 'api')
            return [
                'id' => 'ID',
                'user_id' => 'user_id',
                'amount' => 'amount',
                'status' => 'status',
                'payment_type' => 'payment_type',
                'payment_account' => 'payment_account',
                'note' => 'note',
                'created' => 'created',
                'changed' => 'changed',
            ];

        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'application_id' => 'ID приложения',
            'amount' => 'Сумма',
            'status' => 'Статус',
            'payment_type' => 'Платежная система',
            'payment_account' => 'Кошелек',
            'note' => 'Заметка',
            'created' => 'Создана',
            'changed' => 'Изменена',
        ];
    }

    static public function getStatusesLabels()
    {
        return [
            static::STATUS_WAITING => 'Ожидает',
            static::STATUS_EXECUTING => 'Выполняется',
            static::STATUS_DONE => 'Выполнен',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->getUser()) {
            $this->application_id = $this->getUser()->application_id;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->getUser()->balance -= $this->amount;
            $this->getUser()->save();
        }
        elseif (@$changedAttributes['amount']) {
            $this->getUser()->balance -= $this->amount - @$changedAttributes['amount'];
            $this->getUser()->save();
        }
    }

    public function afterDelete()
    {
        parent::afterDelete();

        $this->getUser()->balance += $this->amount;
        $this->getUser()->save();
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user_id = $user->id;
        $this->_user = $user;
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->_user)
            return $this->_user;

        return $this->_user = User::find()->where(['id'=>$this->user_id])->one();
    }

    /**
     * Переустанавливает ID приложений беря их из указанного юзера.
     * @return int
     * @throws \yii\db\Exception
     */
    public static function resetApplicationsIds()
    {
        $command = (new Query())->createCommand();
        $command->sql = '
            UPDATE payment LEFT JOIN `user` ON (payment.user_id = user.id)
            SET payment.application_id = `user`.application_id
        ';

        return $command->execute();
    }
}
