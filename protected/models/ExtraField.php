<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "extra_field".
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $input
 *
 * @property TaskType[] $taskTypes
 */
class ExtraField extends \yii\db\ActiveRecord
{

    const TEXT_INPUT = 'textInput';
    const TEXT_AREA = 'textarea';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'extra_field';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'label', 'input'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'label' => 'Label',
            'input' => 'Input',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskTypes()
    {
        return $this->hasMany(TaskType::className(), ['id' => 'task_id'])
            ->viaTable('task_type_extra_field', ['extra_field_id' => 'id']);
    }
}
