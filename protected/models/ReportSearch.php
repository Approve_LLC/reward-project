<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;
use yii\db\Expression;

/**
 * ReportSearch represents the model behind the search form of `app\models\Report`.
 */
class ReportSearch extends Report
{
    public $unique_id_not_null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'task_type_id', 'amount'], 'integer'],
            [['unique_id'], 'string'],
            [['unique_id_not_null'], 'integer'],
            [['date_start', 'date_end', 'created', 'changed'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'unique_id' => $this->unique_id,
            'user_id' => $this->user_id,
            'task_type_id' => $this->task_type_id,
            'amount' => $this->amount,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'created' => $this->created,
            'changed' => $this->changed,
        ]);

        if ($this->unique_id_not_null)
            $query->andWhere(new Expression('unique_id IS NOT NULL'));

        return $dataProvider;
    }
}