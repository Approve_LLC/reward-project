<?php

use yii\db\Migration;

/**
 * Class m180626_213045_globalLimitsField
 */
class m180626_213045_globalLimitsField extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_type', 'limit', $this->integer(11));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180626_213045_globalLimitsField was reverted.\n";
        $this->dropColumn('task_type', 'limit');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180626_213045_globalLimitsField cannot be reverted.\n";

        return false;
    }
    */
}
