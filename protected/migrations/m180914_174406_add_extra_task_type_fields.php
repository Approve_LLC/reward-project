<?php

use yii\db\Migration;

/**
 * Class m180914_174406_add_extra_task_type_fields
 */
class m180914_174406_add_extra_task_type_fields extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('extra_field', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256),
            'label' => $this->string(256),
            'input' => $this->string(256),
            'frequency' => $this->integer(11)->defaultValue(0)
        ]);

        $this->createTable('task_type_extra_field', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(11),
            'extra_field_id' => $this->integer(11)
        ]);

        $this->addForeignKey(
            'fk-task_type_extra_field-task_id',
            'task_type_extra_field',
            'task_id',
            'task_type',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-task_type_extra_field-extra_field_id',
            'task_type_extra_field',
            'extra_field_id',
            'extra_field',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-task_type_extra_field-extra_field_id', 'task_type_extra_field');
        $this->dropForeignKey('fk-task_type_extra_field-task_id', 'task_type_extra_field');
        $this->dropTable('task_type_extra_field');
        $this->dropTable('extra_field');
    }
}
