<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180502_000000_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('application', 'register_with_promocode_reward', Schema::TYPE_DECIMAL.'(20,10) NOT NULL DEFAULT 0');
        $this->addColumn('application', 'tasks_to_earn_referer_reward', Schema::TYPE_INTEGER.' NOT NULL DEFAULT 100');
        $this->addColumn('application', 'first_referer_reward', Schema::TYPE_DECIMAL.'(20,10) NOT NULL DEFAULT 0');
        $this->addColumn('application', 'next_referer_rewards', Schema::TYPE_DECIMAL.'(20,10) NOT NULL DEFAULT 0');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'register_with_promocode_reward');
        $this->dropColumn('application', 'tasks_to_earn_referer_reward');
        $this->dropColumn('application', 'first_referer_reward');
        $this->dropColumn('application', 'next_referer_rewards');
    }
}
