<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180223_000000_task_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('task_type', [
            'id' => Schema::TYPE_PK,
            'application_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'min_period' => Schema::TYPE_INTEGER . ' NOT NULL',
            'amount' => Schema::TYPE_DECIMAL. '(20,10) NOT NULL',
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'changed' => 'TIMESTAMP NULL DEFAULT NULL',
        ]);

        $this->addForeignKey('fk_application_id', 'task_type', 'application_id', 'application', 'id');

        $this->insert('task_type', [
            'application_id' => 1,
            'min_period' => 10,
            'amount' => 50,
            'name' => 'test',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('task_type');
    }
}
