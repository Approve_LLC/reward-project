<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180225_000000_payment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('payment', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'application_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'amount' => Schema::TYPE_DECIMAL. '(20,10) NOT NULL',
            'status' => 'ENUM("waiting","executing","done") NOT NULL',
            'payment_type' => Schema::TYPE_STRING . '(64) NOT NULL',
            'payment_account' => Schema::TYPE_STRING . '(64) NOT NULL',
            'note' => Schema::TYPE_STRING . '(255) NULL DEFAULT NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'changed' => 'TIMESTAMP NULL DEFAULT NULL',
        ]);

        $this->addForeignKey('fk_user_id', 'payment', 'user_id', 'user', 'id');
        $this->createIndex('ix_application_id', 'payment', 'application_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('payment');
    }
}
