<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class m180212_074108_user
 */
class m180212_074108_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        //set static db name in command because I can't get database name from db config
        $this->execute('ALTER DATABASE ostap88366_rewrd CHARACTER SET utf8 COLLATE utf8_general_ci;');

        $this->createTable('user', [
            'id' => Schema::TYPE_PK,
            'application_id' => Schema::TYPE_INTEGER . ' unsigned NULL DEFAULT NULL',
            'type' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 3',
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 1',
            'login' => Schema::TYPE_STRING . '(128) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . '(128) NOT NULL',
            'promo_code' => Schema::TYPE_STRING . '(64) NULL DEFAULT NULL',
            'referrer_user_id' => Schema::TYPE_INTEGER . ' unsigned NULL DEFAULT NULL',
            'access_token' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL',
            'password_restore_code' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL',
            'balance' => Schema::TYPE_DECIMAL. '(20,10) NULL DEFAULT 0',
            'note' => Schema::TYPE_TEXT. ' NULL DEFAULT NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
        ]);

        $this->createIndex('ux_app_id_login', 'user', ['application_id','login'], true);
        $this->createIndex('ux_access_token', 'user', 'access_token', true);
        $this->createIndex('ux_app_id_promo_code', 'user', ['application_id','promo_code'], true);

        $this->insert('user', [
            'type' => 0, // admin
            'login' => 'admin@admin.ru',
            // password '123'
            'password_hash' => '$2y$13$viMVf2RzxV7wdzBLEPS6RuBnemVD/IhCziU6LKzgUthag4F6I/yom',
        ]);
        $this->insert('user', [
            'type' => 3, // user
            'application_id' => 1,
            'login' => 'user',
            // password '123'
            'password_hash' => '$2y$13$viMVf2RzxV7wdzBLEPS6RuBnemVD/IhCziU6LKzgUthag4F6I/yom',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
