<?php

use yii\db\Migration;

/**
 * Class m180626_230503_globalLimitCountField
 */
class m180626_230503_globalLimitCountField extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_type', 'limitCount', $this->integer(11)->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180626_230503_globalLimitCountField was reverted.\n";
        $this->dropColumn('task_type', 'limitCount');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180626_230503_globalLimitCountField cannot be reverted.\n";

        return false;
    }
    */
}
