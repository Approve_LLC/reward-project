<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180224_000000_report extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('report', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'task_type_id' => Schema::TYPE_INTEGER . ' NULL DEFAULT NULL',
            'amount' => Schema::TYPE_DECIMAL. '(20,10) NOT NULL',
            'date_start' => 'TIMESTAMP NULL DEFAULT NULL',
            'date_end' => 'TIMESTAMP NULL DEFAULT NULL',
            'unique_id' => Schema::TYPE_STRING . '(64) NULL DEFAULT NULL',
            'info' => Schema::TYPE_TEXT. ' NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'changed' => 'TIMESTAMP NULL DEFAULT NULL',
        ]);

        $this->createIndex('ux_user_unique_id', 'report', 'unique_id', true);
        $this->addForeignKey('fk_user_id2', 'report', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_task_type_id', 'report', 'task_type_id', 'task_type', 'id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('report');
    }
}
