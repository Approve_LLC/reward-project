<?php

use yii\db\Migration;

/**
 * Class m180624_172229_extraFieldsTable
 */
class m180624_172229_extraFieldsTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('sub_task', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer(11)->notNull(),
            'parent_id' => $this->integer(11),
            'custom_data' => $this->integer(11),
            'amount' => $this->integer(11),
            'type' => $this->integer(11)
        ]);

        $this->addForeignKey(
            'fk-sub_task-task_id',
            'sub_task',
            'task_id',
            'task_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180624_172229_extraFieldsTable was reverted.\n";
        $this->dropForeignKey('fk-sub_task-task_id', 'sub_task');
        $this->dropTable('sub_task');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_172229_extraFieldsTable cannot be reverted.\n";

        return false;
    }
    */
}
