<?php

use yii\db\Migration;

/**
 * Class m180625_164444_addSubTaskField
 */
class m180625_164444_addSubTaskField extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('report', 'subtask_id', $this->integer(11)->after('task_type_id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180625_164444_addSubTaskField was reverted.\n";
        $this->dropColumn('report', 'subtask_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180625_164444_addSubTaskField cannot be reverted.\n";

        return false;
    }
    */
}
