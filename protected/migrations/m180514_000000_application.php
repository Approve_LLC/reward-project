<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180514_000000_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('application', 'task_fields_set', Schema::TYPE_STRING.'(64) NOT NULL DEFAULT ""');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'task_fields_set');
    }
}
