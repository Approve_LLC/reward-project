<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180222_000000_application extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('application', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(64) NOT NULL',
            'secret' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL',
            'adcolony_key' => Schema::TYPE_STRING . '(128) NULL DEFAULT NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'changed' => 'TIMESTAMP NULL DEFAULT NULL',
        ]);

        $this->insert('application', [
            'name' => 'test_app',
            'secret' => '1234SecRet777xG',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('application');
    }
}
