<?php

use yii\db\Migration;

/**
 * Class m180915_055923_add_time_interval_to_sub_tasks
 */
class m180915_055923_add_time_interval_to_sub_tasks extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sub_task', 'interval', $this->integer(11)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sub_task', 'interval');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180915_055923_add_time_interval_to_sub_tasks cannot be reverted.\n";

        return false;
    }
    */
}
