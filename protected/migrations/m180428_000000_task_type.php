<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180428_000000_task_type extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_type', 'custom_data', Schema::TYPE_TEXT . ' NULL DEFAULT NULL after name');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('task_type', 'custom_data');
    }
}
