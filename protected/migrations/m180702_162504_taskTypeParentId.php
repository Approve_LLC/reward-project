<?php

use yii\db\Migration;

/**
 * Class m180702_162504_taskTypeParentId
 */
class m180702_162504_taskTypeParentId extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('task_type', 'parent_id', $this->integer(11)->after('id')->defaultValue(NULL));

        $this->addForeignKey(
            'fk-task_type-parent_id',
            'task_type',
            'parent_id',
            'task_type',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-task_type-parent_id',
            'task_type'
        );
        $this->dropColumn('task_type', 'parent_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180702_162504_taskTypeParentId cannot be reverted.\n";

        return false;
    }
    */
}
