<?php

use yii\db\Migration;

/**
 * Class m190209_161943_add_version_field
 */
class m190209_161943_add_version_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('application', 'version', $this->float()->defaultValue(1.0));
        $this->addColumn('application', 'force', $this->boolean()->defaultValue(false));
        $this->addColumn('application', 'update_message', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('application', 'version');
        $this->dropColumn('application', 'force');
        $this->dropColumn('application', 'update_message');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190209_161943_add_version_field cannot be reverted.\n";

        return false;
    }
    */
}
