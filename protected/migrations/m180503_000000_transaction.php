<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

/**
 * Class
 */
class m180503_000000_transaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => "ENUM('promocode_register_bonus','referral_reward') NULL DEFAULT NULL",
            'amount' => Schema::TYPE_DECIMAL. '(20,10) NOT NULL',
            'info' => Schema::TYPE_TEXT. ' NULL',
            'created' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'changed' => 'TIMESTAMP NULL DEFAULT NULL',
        ]);

        $this->createIndex('ix_user_id', 'transaction', 'user_id');
        $this->createIndex('ix_type', 'transaction', 'type');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('transaction');
    }
}
