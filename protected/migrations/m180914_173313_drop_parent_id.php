<?php

use yii\db\Migration;

/**
 * Class m180914_173313_drop_parent_id
 */
class m180914_173313_drop_parent_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-task_type-parent_id',
            'task_type'
        );
        $this->dropColumn('task_type', 'parent_id');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->addColumn('task_type', 'parent_id', $this->integer(11)->after('id')->defaultValue(NULL));

        $this->addForeignKey(
            'fk-task_type-parent_id',
            'task_type',
            'parent_id',
            'task_type',
            'id',
            'CASCADE'
        );
    }
    
}
