<?php

use yii\db\Migration;

/**
 * Class m180911_201427_change_subtask_amount_field
 */
class m180911_201427_change_subtask_amount_field extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('sub_task', 'amount', $this->double()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn('sub_task', 'amount', $this->integer(11)->after('id')->defaultValue(0));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180911_201427_change_subtask_amount_field cannot be reverted.\n";

        return false;
    }
    */
}
