<?php

use yii\db\Migration;

/**
 * Class m190130_185747_add_necesasary_foreigns
 */
class m190130_185747_add_necesasary_foreigns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-transaction-user_id',
            'transaction',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190130_185747_add_necesasary_foreigns cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190130_185747_add_necesasary_foreigns cannot be reverted.\n";

        return false;
    }
    */
}
