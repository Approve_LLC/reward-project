<?php

use yii\db\Migration;

/**
 * Class m190219_111358_change_verion_column
 */
class m190219_111358_change_verion_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('application', 'version', $this->string(512));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('application', 'version', $this->float()->defaultValue(1.0));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_111358_change_verion_column cannot be reverted.\n";

        return false;
    }
    */
}
