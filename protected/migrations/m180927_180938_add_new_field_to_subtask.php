<?php

use yii\db\Migration;

/**
 * Class m180927_180938_add_new_field_to_subtask
 */
class m180927_180938_add_new_field_to_subtask extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sub_task', 'extra_field', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sub_task', 'extra_field');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180927_180938_add_new_field_to_subtask cannot be reverted.\n";

        return false;
    }
    */
}
