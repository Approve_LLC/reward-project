<?php

namespace app\modules\v2;

/**
 * @OA\Info(
 *     version="2.0",
 *     title="Reward APIDOC"
 * )
 */
/**
 * @OA\SecurityScheme(
 *     securityScheme="bearer",
 *     scheme="bearer",
 *     type="http",
 *     name="Authorization",
 *     in="header"
 * ),
 */

/**
 * Class Module
 * @package app\modules\v2
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }
}