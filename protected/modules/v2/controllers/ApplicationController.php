<?php

namespace app\modules\v2\controllers;

use yii\rest\ActiveController;

/**
 * Class ApplicationController
 * @package app\modules\v2\controllers
 */
class ApplicationController extends ActiveController
{
    public $modelClass = 'app\models\Application';

    /**
     * @OA\Get(
     *     path="/v2/applications/{appId}",
     *     tags={"application"},
     *     summary="Экземпляр приложения.",
     *     description="Для получения отдельных полей через метод, например, GET /v2/applications можно так: GET /v2/applications?fields=id,name",
     *     @OA\Parameter(
     *        in = "path",
     *        name = "appId",
     *        description = "ID приложения",
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Возвращает экземпляр приложения.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Application")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized",
     *     ),
     * )
     */
}