<?php

namespace app\modules\v2\controllers;

use app\models\ApiLoginForm;
use app\models\RestorePasswordForm;
use app\models\RestorePasswordViaEmailForm;
use app\models\User;
use Yii;
use yii\rest\Controller;

/**
 * Class AuthController
 * @package app\modules\v2\controllers
 */
class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/v2/auth/login",
     *     tags={"auth"},
     *     summary="Авторизация. Получение идентификатора сессии.",
     *     @OA\RequestBody(
     *         request="Login",
     *         description="Поля необходимые для входа.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Авторизация. Получение идентификатора сессии.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Login")
     *         )
     *     ),
     * )
     */
    public function actionLogin()
    {
        $form = new ApiLoginForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if ($form->validate()) {
            $result['status'] = 0;
            $result['session_id'] = $form->getUser()->access_token;
            $result['user_id'] = $form->getUser()->id;
        } else {
            $result['status'] = $form->status;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        }

        return $result;
    }

    /**
     * @OA\Post(
     *     path="/v2/auth/register",
     *     tags={"auth"},
     *     summary="Регистрация.",
     *     @OA\RequestBody(
     *         request="Register",
     *         description="Поля необходимые для регистрации.",
     *         @OA\JsonContent(ref="#/components/schemas/RegisterForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Регистрация.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Register")
     *         )
     *     ),
     * )
     */
    public function actionRegister()
    {
        $user = new User;
        $user->application_id = Yii::$app->request->post('app_id');

        if ($user->load(Yii::$app->request->post(), '') && $user->save())
            return [
                'status' => 0,
                'message' => 'Регистрация прошла успешно.',
                'session_id' => $user->makeAccessToken(),
                'user_id' => $user->id
            ];
        else
            return ['status' => 1, 'message' => @array_values($user->getFirstErrors())[0]
                ?: 'Необходимы: login, password, app_id.'];
    }

    /**
     * @OA\Post(
     *     path="/v2/auth/restore-password-via-email",
     *     tags={"auth"},
     *     summary="Высылает на емеил ссылку на восстановление пароля.",
     *     @OA\RequestBody(
     *         request="RestorePasswordViaEmail",
     *         description="Поля необходимые для получения кода для смены пароля.",
     *         @OA\JsonContent(ref="#/components/schemas/RestorePasswordViaEmailForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Высылает на емеил ссылку на восстановление пароля.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RestorePasswordViaEmail")
     *         )
     *     ),
     * )
     */
    public function actionRestorePasswordViaEmail()
    {
        $form = new RestorePasswordViaEmailForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if (!$form->validate()) {
            $result['status'] = 1;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        } elseif ($form->getUser()->sendRestorePasswordEmail()) {
            $result['status'] = 0;
            $result['message'] = 'На указанный email выслан код для восстановления пароля.';
        } else {
            $result['status'] = 1;
            $result['message'] = 'Не удалось отправить Email.';
        }

        return $result;
    }

    /**
     * @OA\Post(
     *     path="/v2/auth/restore-password",
     *     tags={"auth"},
     *     summary="Меняет пароль пользователя.",
     *     @OA\RequestBody(
     *         request="RestorePassword",
     *         description="Поля необходимые для смены пароля.",
     *         @OA\JsonContent(ref="#/components/schemas/RestorePasswordForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Меняет пароль пользователя.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RestorePassword")
     *         )
     *     ),
     * )
     */
    public function actionRestorePassword()
    {
        $form = new RestorePasswordForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if (!$form->validate()) {
            $result['status'] = 1;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        } elseif ($form->saveNewPassword()) {
            $result['status'] = 0;
            $result['message'] = 'Новый пароль успешно сохранен.';
        } else {
            $result['status'] = 1;
            $result['message'] = 'Не удалось сменить пароль.';
        }

        return $result;
    }
}