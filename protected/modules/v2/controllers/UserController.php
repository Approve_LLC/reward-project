<?php

namespace app\modules\v2\controllers;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use app\models\User;

/**
 * Class UserController
 * @package app\modules\v2\controllers
 */
class UserController extends Controller
{
    public $modelClass = 'app\models\User';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class,
            'except' => [
                'get-time',
            ],
        ];

        return $behaviors;
    }

    /**
     * @OA\Get(
     *     path="/v2/user",
     *     tags={"user"},
     *     summary="Выдает id, balance, promoCode, реферралов и доход полученный от них пользователю.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает id, balance, promoCode, реферралов и доход полученный от них пользователю.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/User")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionView()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        return [
            'id'=> $user->id,
            'balance'=> $user->balance,
            'promoCode'=> $user->promoCode,
            'countReferrers' => $user->countReferrers(),
            'sumRewardsForReferrers' => $user->getSumRewardsForReferrers()
        ];
    }

    /**
     * @OA\Get(
     *     path="/v2/user/get-time",
     *     tags={"user"},
     *     summary="Выдает timestamp в миллисекундах.",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает число",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     * )
     */
    public function actionGetTime()
    {
        $dbTime = (new Query())->select(new Expression('NOW()'))->scalar();

        if (Yii::$app->request->get('ms'))
            return strtotime($dbTime) * 1000;
        else
            return strtotime($dbTime);
    }

    /**
     * @OA\Post(
     *     path="/v2/user/set-promocode",
     *     tags={"user"},
     *     summary="Устанавливает новый промокод пользователя.",
     *     description="",
     *     @OA\RequestBody(
     *         request="SetPromocode",
     *         description="Укажите промокод.",
     *         @OA\JsonContent(ref="#/components/schemas/SetPromocodeForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/SetPromocode")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionSetPromocode()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        $user->promoCode = Yii::$app->request->post('promocode');

        if ($user->save()){
            return ['status' => 0, 'message' => "Промокод установлен."];
        }
    }
}