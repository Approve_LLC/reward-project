<?php

namespace app\modules\v2\controllers;

use app\models\SubTask;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use app\models\Report;
use app\models\TaskType;
use app\models\User;

/**
 * Class TaskTypeController
 * @package app\modules\v2\controllers
 */
class TaskTypeController extends Controller
{
    public $modelClass = 'app\models\TaskType';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class
        ];

        return $behaviors;
    }

    /**
     * @OA\Get(
     *     path="/v2/task-types",
     *     tags={"task type"},
     *     summary="Выдает список типов заданий, с id,
    названиями и временем в секундах, которое надо подождать, перед тем как задание станет доступно.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает список типов заданий, с id, названиями и временем в секундах, которое надо подождать, перед тем как задание станет доступно.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ListTaskTypes")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionIndex()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        return TaskType::getListForUser($user);
    }

    /**
     * @OA\Get(
     *     path="/v2/task-types/{taskId}/subtasks",
     *     tags={"task type"},
     *     summary="Выдает подзадания пользователя по id задания. Ничего не вернет, если не сделаное главное задание. Возвращает все по определенному в админке интервалу.",
     *     description="",
     *     @OA\Parameter(
     *        in = "path",
     *        name = "taskId",
     *        description = "ID задания",
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает подзадания пользователя.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Subtasks")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionGetSubtasks($taskId)
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return SubTask::getSubTasksForUserByTaskId($user, $taskId);
    }

    /**
     * @OA\Delete(
     *     path="/v2/task-types/{taskTypeId}/reports",
     *     tags={"task type"},
     *     summary="Удаляет репорты с определенным task_type_id и пересчитывает баланс пользователя.",
     *     description="",
     *     @OA\RequestBody(
     *         request="DeleteReports",
     *         description="Укажите task_type_id.",
     *         @OA\JsonContent(ref="#/components/schemas/DeleteReportsForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/DeleteReports")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionDeleteReports($taskTypeId)
    {
        $deleteCount = Report::deleteAll(['task_type_id' => $taskTypeId]);
        TaskType::decrementLimit($taskTypeId, $deleteCount);
        try {
            User::recalculateBalances();
        } catch (\Exception $e) {
            return ['status' => 0, 'message' => "Internal error"];
        }
        return ['status' => 0, 'message' => "Всего строк удалено: " . $deleteCount];
    }

}