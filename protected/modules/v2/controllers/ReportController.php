<?php

namespace app\modules\v2\controllers;

use app\models\Report;
use app\models\SubTask;
use app\models\TaskType;
use app\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * Class ReportController
 * @package app\modules\v2\controllers
 */
class ReportController extends Controller
{
    public $modelClass = 'app\models\Report';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class
        ];

        return $behaviors;
    }

    /**
     * @OA\Post(
     *     path="/v2/reports",
     *     tags={"report"},
     *     summary="Принимает отчет о выполнении задания.",
     *     description="",
     *     @OA\RequestBody(
     *         request="SaveReport",
     *         description="Поля необходимые для получения того, чтобы сохранить отчет о задании или подзадании.",
     *         @OA\JsonContent(ref="#/components/schemas/SaveReportForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Принимает отчет о выполнении задания.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Report")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionCreate()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        $report = new Report(['scenario' => 'api']);

        if ($report->load(Yii::$app->request->post(), '')) {
            $report->setUser($user);
            if (empty($report->setAmountFromSubtask())) {
                $report->setAmountFromTaskType();
            }
            if($report->validate()) {
                if (!TaskType::checkLimit($report->task_type_id)) {
                    return ['status' => 1, 'message' => 'Лимит превышен'];
                }
                if ($report->save()) {
                    if (!empty($report->subtask_id)) {
                        $column = SubTask::find()->where(['task_id' => $report->task_type_id])->max('id');
                        if ($report->subtask_id == $column) {
                            TaskType::incrementLimit($report->task_type_id);
                        }
                    }
                    $amount = preg_replace('/\.0+$/', '', $report->amount); // trim zeros
                    return ['status' => 0, 'message' => "Вам начислены баллы.", 'amount' => $amount];
                } else {
                    return ['status' => 1, 'message' => @array_values($report->getFirstErrors())[0]];
                }
            }
        }
        else {
            return ['status' => 1, 'message' => $report->errors];
        }
    }
}