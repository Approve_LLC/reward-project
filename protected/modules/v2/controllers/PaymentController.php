<?php

namespace app\modules\v2\controllers;

use app\models\Payment;
use app\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;

/**
 * Class PaymentController
 * @package app\modules\v2\controllers
 */
class PaymentController extends Controller
{
    public $modelClass = 'app\models\Payment';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::class
        ];

        return $behaviors;
    }

    /**
     * @OA\Get(
     *     path="/v2/payments",
     *     tags={"payment"},
     *     summary="Выдает 100 последних выплат пользователя.",
     *     description="",
     *     @OA\Parameter(
     *        in = "query",
     *        name = "status",
     *        description = "Необязательный параметр, для выборки выплат с определенным статусом.
    У каждой выплаты есть status, который может быть равен:
    awaiting  - ожидает
    executing - выполняется
    done      - выполнена",
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/GetPayments")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionIndex()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        $query = Payment::find()
            ->select(['id', 'status', 'amount', 'payment_type', 'payment_account'])
            ->where(['user_id' => $user->id]);
        if (!empty(Yii::$app->request->get('status'))) {
            $query = $query->andWhere([
                'status' => Yii::$app->request->get('status')
            ]);
        }
        $query = $query->orderBy('id')
            ->limit(100)
            ->asArray()
            ->all();
        return $query;
    }

    /**
     * @OA\Post(
     *     path="/v2/payments",
     *     tags={"payment"},
     *     summary="Создает выплату указанной суммы, на указанный счёт.",
     *     description="",
     *     @OA\RequestBody(
     *         request="CreatePayment",
     *         description="Поля необходимые для получения того, чтобы создать запрос на выплату.",
     *         @OA\JsonContent(ref="#/components/schemas/CreatePaymentForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Создает выплату указанной суммы, на указанный счёт.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/CreatePayment")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Unauthorized",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionCreate()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        $payment = new Payment(['scenario' => 'api']);
        $payment->load(Yii::$app->request->post(), '');
        $payment->setUser($user);


        if ($payment->save())
            return ['status' => 0, 'message' => "Выплата поставлена в очередь."];
        else
            return ['status' => 1, 'message' => @array_values($payment->getFirstErrors())[0]];
    }

}