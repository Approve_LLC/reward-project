<?php

namespace app\modules\v1;

/**
 * @OA\Info(
 *     version="1.0",
 *     title="Reward APIDOC"
 * )
 */
/**
 * @OA\SecurityScheme(
 *     securityScheme="bearer",
 *     scheme="bearer",
 *     type="http",
 *     name="Authorization",
 *     in="header"
 * ),
 */
/**
 * Class Module
 * @package app\modules\v1
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
    }
}