<?php

namespace app\modules\v1\controllers;

use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;
use yii\web\Response;

use app\models\ApiLoginForm;
use app\models\Payment;
use app\models\Report;
use app\models\RestorePasswordForm;
use app\models\RestorePasswordViaEmailForm;
use app\models\TaskType;
use app\models\User;
use app\models\SubTask;

/**
 * Class ApiController
 * @package app\modules\v1\controllers
 */
class ApiController extends Controller
{
    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();

        // no session cookie
        Yii::$app->user->enableSession = false;

        // json output
        $response = Yii::$app->getResponse();
        $response->format = Response::FORMAT_JSON;

        // log
        Yii::info([
            Yii::$app->requestedRoute,
            Yii::$app->request->headers->get('Authorization')
        ], 'api');
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        // log
        Yii::info([
            $action->id,
            'status code: ' . Yii::$app->getResponse()->getStatusCode(),
            $result
        ], 'api_response');

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['bearerAuth'] = [
            'class' => HttpBearerAuth::className(),
            'except' => [
                'login',
                'register',
                'restore-password',
                'restore-password-via-email',
                'get-time',
                'appodeal-report',
                'applovin-report',
                'adcolony-report',
                'chartboost-report'
            ],
        ];

        return $behaviors;
    }

    /**
     * @OA\Post(
     *     path="/v1/api/login",
     *     tags={"api"},
     *     summary="Авторизация. Получение идентификатора сессии.",
     *     @OA\RequestBody(
     *         request="Login",
     *         description="Поля необходимые для входа.",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/LoginForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Авторизация. Получение идентификатора сессии.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Login")
     *         )
     *     ),
     * )
     */
    public function actionLogin()
    {
        $form = new ApiLoginForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if ($form->validate()) {
            $result['status'] = 0;
            $result['session_id'] = $form->getUser()->makeAccessToken();
            $result['user_id'] = $form->getUser()->id;
        } else {
            $result['status'] = $form->status;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        }

        return $result;
    }

    /**
     * @OA\Post(
     *     path="/v1/api/register",
     *     tags={"api"},
     *     summary="Регистрация.",
     *     @OA\RequestBody(
     *         request="Register",
     *         description="Поля необходимые для регистрации.",
     *         @OA\JsonContent(ref="#/components/schemas/RegisterForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Регистрация.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Register")
     *         )
     *     ),
     * )
     */
    public function actionRegister()
    {
        /**
         * todo: создать и использовать тут ApiRegistrationForm чтоб названия полей в message были на английском и application_id отдельно не присваивать
         * todo: !! ограничить $user->load() чтобы затрагивались только нужные атрибуты
         */
        $user = new User;
        $user->application_id = Yii::$app->request->post('app_id');

        if ($user->load(Yii::$app->request->post(), '') && $user->save())
            return ['status' => 0, 'message' => 'Регистрация прошла успешно.', 'user_id' => $user->id];
        else
            return ['status' => 1, 'message' => @array_values($user->getFirstErrors())[0]
                ?: 'Необходимы: login, password, app_id.'];
    }

    /**
     * @OA\Post(
     *     path="/v1/api/restore-password-via-email",
     *     tags={"api"},
     *     summary="Высылает на емеил ссылку на восстановление пароля.",
     *     @OA\RequestBody(
     *         request="RestorePasswordViaEmail",
     *         description="Поля необходимые для получения кода для смены пароля.",
     *         @OA\JsonContent(ref="#/components/schemas/RestorePasswordViaEmailForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Высылает на емеил ссылку на восстановление пароля.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RestorePasswordViaEmail")
     *         )
     *     ),
     * )
     */
    public function actionRestorePasswordViaEmail()
    {
        $form = new RestorePasswordViaEmailForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if (!$form->validate()) {
            $result['status'] = 1;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        } elseif ($form->getUser()->sendRestorePasswordEmail()) {
            $result['status'] = 0;
            $result['message'] = 'На указанный email выслан код для восстановления пароля.';
        } else {
            $result['status'] = 1;
            $result['message'] = 'Не удалось отправить Email.';
        }

        return $result;
    }

    /**
     * @OA\Post(
     *     path="/v1/api/restore-password",
     *     tags={"api"},
     *     summary="Меняет пароль пользователя.",
     *     @OA\RequestBody(
     *         request="RestorePassword",
     *         description="Поля необходимые для смены пароля.",
     *         @OA\JsonContent(ref="#/components/schemas/RestorePasswordForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Меняет пароль пользователя.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/RestorePassword")
     *         )
     *     ),
     * )
     */
    public function actionRestorePassword()
    {
        $form = new RestorePasswordForm;
        $form->load(Yii::$app->request->post(), '');

        $result = [];

        if (!$form->validate()) {
            $result['status'] = 1;
            $result['message'] = @array_values($form->getFirstErrors())[0];
        } elseif ($form->saveNewPassword()) {
            $result['status'] = 0;
            $result['message'] = 'Новый пароль успешно сохранен.';
        } else {
            $result['status'] = 1;
            $result['message'] = 'Не удалось сменить пароль.';
        }

        return $result;
    }

    /**
     * @OA\Get(
     *     path="/v1/api/list-task-types",
     *     tags={"api"},
     *     summary="Выдает список типов заданий, с id,
    названиями и временем в секундах, которое надо подождать, перед тем как задание станет доступно.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает список типов заданий, с id, названиями и временем в секундах, которое надо подождать, перед тем как задание станет доступно.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ListTaskTypes")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionListTaskTypes()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        return TaskType::getListForUser($user);
    }

    /**
     * @OA\Get(
     *     path="/v1/api/subtasks",
     *     tags={"api"},
     *     summary="Выдает подзадания пользователя.",
     *     description="",
     *     @OA\Parameter(
     *        in = "query",
     *        name = "taskId",
     *        description = "ID задания",
     *        @OA\Schema(
     *             type="integer"
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает подзадания пользователя.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Subtasks")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionSubtasks($taskId)
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        return SubTask::getSubTasksForUserByTaskId($user, $taskId);
    }

    /**
     * @OA\Post(
     *     path="/v1/api/save-report",
     *     tags={"api"},
     *     summary="Принимает отчет о выполнении задания.",
     *     description="Пример хэша: d0fecfb92f053ebbbed5448e764a48dc
    Пример строки для хэширования: 122018-02-20T01:59:042018-02-20T02:10:041234SecRet777xG",
     *     @OA\RequestBody(
     *         request="SaveReport",
     *         description="Поля необходимые для получения того, чтобы сохранить отчет о задании или подзадании.",
     *         @OA\JsonContent(ref="#/components/schemas/SaveReportForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Принимает отчет о выполнении задания.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/SaveReport")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionSaveReport()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        $report = new Report(['scenario' => 'api']);
        $report->load(Yii::$app->request->post(), '');
        if (empty($report->setAmountFromSubtask())) {
            $report->setAmountFromTaskType();
        }
        $report->setUser($user);
        if (!TaskType::checkLimit($report->task_type_id)) {
            return ['status' => 1, 'message' => 'Лимит превышен'];
        }
        if ($report->save()) {
            if (!empty($report->subtask_id)) {
                $column = SubTask::find()->where(['task_id' => $report->task_type_id])->max('id');
                if ($report->subtask_id == $column) {
                    TaskType::incrementLimit($report->task_type_id);
                }
            }
            $amount = preg_replace('/\.0+$/', '', $report->amount); // trim zeros
            return ['status' => 0, 'message' => "Вам начислено $amount баллов."];
        } else {
            return ['status' => 1, 'message' => @array_values($report->getFirstErrors())[0]];
        }
    }


    /**
     * @OA\Post(
     *     path="/v1/api/create-payment",
     *     tags={"api"},
     *     summary="Создает выплату указанной суммы, на указанный счёт.",
     *     description="",
     *     @OA\RequestBody(
     *         request="CreatePayment",
     *         description="Поля необходимые для получения того, чтобы создать запрос на выплату.",
     *         @OA\JsonContent(ref="#/components/schemas/CreatePaymentForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Создает выплату указанной суммы, на указанный счёт.",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/CreatePayment")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden",
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionCreatePayment()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();

        $payment = new Payment(['scenario' => 'api']);
        $payment->load(Yii::$app->request->post(), '');
        $payment->setUser($user);


        if ($payment->save())
            return ['status' => 0, 'message' => "Выплата поставлена в очередь."];
        else
            return ['status' => 1, 'message' => @array_values($payment->getFirstErrors())[0]];
    }

    /**
     * @OA\Get(
     *     path="/v1/api/get-payments",
     *     tags={"api"},
     *     summary="Выдает 100 последних выплат пользователя.",
     *     description="",
     *     @OA\Parameter(
     *        in = "query",
     *        name = "status",
     *        description = "Необязательный параметр, для выборки выплат с определенным статусом.
    У каждой выплаты есть status, который может быть равен:
    awaiting  - ожидает
    executing - выполняется
    done      - выполнена",
     *        @OA\Schema(
     *             type="string"
     *        )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/GetPayments")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionGetPayments()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        $query = Payment::find()
            ->select(['id', 'status', 'amount', 'payment_type', 'payment_account'])
            ->where(['user_id' => $user->id]);
        if (!empty(Yii::$app->request->get('status'))) {
            $query = $query->andWhere([
                'status' => Yii::$app->request->get('status')
            ]);
        }
        $query = $query->orderBy('id')
            ->limit(100)
            ->asArray()
            ->all();
        return $query;
    }

    /**
     * @OA\Get(
     *     path="/v1/api/get-balance",
     *     tags={"api"},
     *     summary="Выдает баланс пользователя.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает целое число",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionGetBalance()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return floatval($user->balance);
    }

    /**
     * @OA\Get(
     *     path="/v1/api/get-user-id",
     *     tags={"api"},
     *     summary="Выдает ID пользователя.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает целое число",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionGetUserId()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return $user->id;
    }

    /**
     * @OA\Get(
     *     path="/v1/api/get-time",
     *     tags={"api"},
     *     summary="Выдает timestamp в миллисекундах.",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает число",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     * )
     */
    public function actionGetTime()
    {
        $dbTime = (new Query())->select(new Expression('NOW()'))->scalar();

        if (Yii::$app->request->get('ms'))
            return strtotime($dbTime) * 1000;
        else
            return strtotime($dbTime);
    }

    /**
     * @OA\Get(
     *     path="/v1/api/get-promocode",
     *     tags={"api"},
     *     summary="Выдает промокод пользователя. Первоначально он равен ID пользователя. Запрос должен содержать заголовок с ID сессии.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Выдает строку или число",
     *         @OA\JsonContent(
     *             type="string"
     *         ),
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionGetPromocode()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return $user->promoCode;
    }

    /**
     * @OA\Post(
     *     path="/v1/api/set-promocode",
     *     tags={"api"},
     *     summary="Устанавливает новый промокод пользователя.",
     *     description="",
     *     @OA\RequestBody(
     *         request="SetPromocode",
     *         description="Укажите промокод.",
     *         @OA\JsonContent(ref="#/components/schemas/SetPromocodeForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/SetPromocode")
     *         )
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionSetPromocode()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        $user->promoCode = Yii::$app->request->post('promocode');

        if ($user->save())
            return ['status' => 0, 'message' => "Промокод установлен."];
        else
            return ['status' => 1, 'message' => @array_values($user->getFirstErrors())[0]];
    }

    /**
     * @OA\Get(
     *     path="/v1/api/count-referrers",
     *     tags={"api"},
     *     summary="Выдает кол-во рефереров - пользователей которые зарегались с промокодом текущего пользователя.",
     *     description="",
     *     @OA\Response(
     *         response = 200,
     *         description = "Возвращает целое число",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionCountReferrers()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return $user->countReferrers();
    }

    /**
     * @OA\Get(
     *     path="/v1/api/sum-rewards-for-referrers",
     *     tags={"api"},
     *     summary="Выдает доход полученный от рефералов.",
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionSumRewardsForReferrers()
    {
        /* @var User $user */
        $user = Yii::$app->user->getIdentity();
        return $user->getSumRewardsForReferrers();
    }

    /**
     * @OA\Delete(
     *     path="/v1/api/delete-reports",
     *     tags={"api"},
     *     summary="Удаляет репорты с определенным task_type_id и пересчитывает баланс пользователя.",
     *     description="",
     *     @OA\RequestBody(
     *         request="DeleteReports",
     *         description="Укажите task_type_id.",
     *         @OA\JsonContent(ref="#/components/schemas/DeleteReportsForm")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/SetPromocode")
     *         )
     *     ),
     *     @OA\Response(
     *         response = 401,
     *         description = "Forbidden"
     *     ),
     *     security={
     *         {"bearer": {}}
     *     }
     * )
     */
    public function actionDeleteReports()
    {
        $postData = Yii::$app->request->post();
        $id = $postData['id'];
        $deleteCount = Report::deleteAll(['task_type_id' => $id]);
        TaskType::decrementLimit($id, $deleteCount);
        try {
            User::recalculateBalances();
        } catch (\Exception $e) {
            return ['status' => 0, 'message' => "Internal error"];
        }
        return ['status' => 0, 'message' => "Всего строк удалено: " . $deleteCount];
    }
}
