<?php

use app\models\Payment;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Выплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Переназначить ID приложений', ['reset-applications-ids'], ['class' => 'btn pull-right', 'title' => 'На основе отчетов и выплат']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            'id',
            'user_id',
            'application_id',
            [
                'attribute' => 'amount',
                'value' => function ($model, $key, $index, $column) {
                    return preg_replace('/\.?0+$/', '', $model->amount); // trim dot&zeros
                },
            ],
            'payment_type',
            'payment_account',
            [
                'attribute' => 'status',
                'filter' => Payment::getStatusesLabels(),
                'value' => function ($model, $key, $index, $grid) {
                    return @Payment::getStatusesLabels()[$model->status];
                }
            ],
            'created',
            //'changed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
