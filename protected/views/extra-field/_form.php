<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ExtraField */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="extra-field-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'input')->dropDownList([
        \app\models\ExtraField::TEXT_INPUT => \app\models\ExtraField::TEXT_INPUT,
        \app\models\ExtraField::TEXT_AREA => \app\models\ExtraField::TEXT_AREA,
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
