<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExtraField */

$this->title = 'Create Extra Field';
$this->params['breadcrumbs'][] = ['label' => 'Extra Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
