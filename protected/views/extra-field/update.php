<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ExtraField */

$this->title = 'Update Extra Field: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Extra Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="extra-field-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
