<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Отчеты о выполнении заданий. При создании и удалении отчетов и выплат, баланс пользователей меняется автоматически.
        <br>
        <?= Html::a('Найти отчеты', ['', 'ReportSearch[unique_id_not_null]'=>1]) ?> присланные на колбеки.
    </p>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => true,
            'lastPageLabel' => true,
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    if ($model->unique_id) {
                        return $model->id . ' <span style="color: #AAAAAA" title="ID от рекламной сети">'
                            . $model->unique_id . '</span>';
                    }
                    return $model->id;
                },
            ],
            'user_id',
            'task_type_id',
	    'subtask_id',
            [
                'attribute' => 'amount',
                'value' => function ($model, $key, $index, $column) {
                    return preg_replace('/\.?0+$/', '', $model->amount); // trim dot&zeros
                },
            ],
            //'date_start',
            //'date_end',
            'created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
