<?php

/* @var $this yii\web\View */

use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Помощь';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        Appodeal S2S Reward Callback Encryption key: <?= Yii::$app->params['appodealEncryptionKey'] ?>
    </p>
    <p>
        Chartboost S2S key: <?= Yii::$app->params['chartboostS2Skey'] ?>
    </p>
    <p>
        &nbsp;
    </p>
    <p>
        Адреса для приёма колбэков с рекламных бирж:
    </p>
    <ul>
        <li><?= Url::to(['api/appodeal-report'], true)?></li>
        <li><?= Url::to(['api/applovin-report'], true)?></li>
        <li><?= Url::to(['api/adcolony-report'], true)?></li>
        <li><?= Url::to(['api/chartboost-report'], true)?></li>
<!--        <li>--><?//= Url::to(['api/vungle-report'], true)?><!--</li>-->
    </ul>
    <p>
        <a href="https://docs.google.com/document/d/1HJ_sRVGHl41jPWviqDvdukAldWeTiD9951GAN1JBUVc/edit">
            Подробности в общей документации.
        </a>
    </p>
    <br>
    <br>
    <br>
    <br>
    <p>
        Ваш IP: <?= Yii::$app->request->getUserIP() ?>
    </p>
    <p>
        Время MySql: <?= (new Query())->select(new Expression('NOW()'))->scalar() ?>
    </p>
    <p>
        Время PHP: <?= date('c') ?>
    </p>
</div>
