<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

use app\models\SubTask;

?>
    <h4>Дочерние задачи:</h4>
<?php Pjax::begin(['enablePushState' => false]); ?>
    <div class="well well-lg">
        <?php $form = ActiveForm::begin([
            'action' => ['add-subtask'],
            'method' => 'post',
            'options' => [
                'data' => [
                    'pjax' => true
                ]
            ],
        ]); ?>
        <div class="row">
            <?= $form->field($subTaskModel, 'task_id')->hiddenInput(['value' => $model->id])->label(false) ?>
            <div class="col-xs-4">
                <?= $form->field($subTaskModel, 'type')->dropDownList([
                    SubTask::TASK_SPENT_TIME => 'Необходимое время',
                    SubTask::TASK_COUNT_ENTRIES => 'Необходимое количество открытий'
                ]); ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTaskModel, 'custom_data')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTaskModel, 'amount')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTaskModel, 'interval')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTaskModel, 'extra_field[time_dimension]')->dropDownList(
                    SubTask::getIntervalDimensions()
                )->label('Ед. измерения интервала') ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-xs-12">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php foreach ($model->subTasks as $index => $subTask): ?>
    <div class="well well-lg">
        <?php $form = ActiveForm::begin([
            'action' => ['update-subtask'],
            'method' => 'post',
            'options' => [
                'data' => [
                    'pjax' => true
                ]
            ],
        ]); ?>
        <div class="row">
            <div class="col-xs-6">
                ID подзадания #<?= $subTask->id ?>
            </div>
        </div>
        <div class="row">
            <?= $form->field($subTask, 'id')->hiddenInput(['value' => $subTask->id])->label(false) ?>
            <?= $form->field($subTask, 'task_id')->hiddenInput(['value' => $model->id])->label(false) ?>
            <div class="col-xs-4">
                <?= $form->field($subTask, 'type')->dropDownList([
                    SubTask::TASK_SPENT_TIME => 'Необходимое время',
                    SubTask::TASK_COUNT_ENTRIES => 'Необходимое количество открытий'
                ]); ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTask, 'custom_data')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTask, 'amount')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTask, 'interval')->textInput() ?>
            </div>
            <div class="col-xs-2">
                <?= $form->field($subTask, 'extra_field[time_dimension]')->dropDownList(
                    SubTask::getIntervalDimensions()
                )->label('Ед. измерения интервала') ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-xs-1">
                    <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary']) ?>
                </div>
                <div class="col-xs-1">
                    <?= Html::a('Удалить', ['task-type/delete-subtask?id=' . $subTask->id . '&taskId=' . $model->id], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php endforeach; ?>
<?php Pjax::end(); ?>