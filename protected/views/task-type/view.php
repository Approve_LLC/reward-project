<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-type-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'application_id',
            'min_period',
            [
                'attribute' => 'amount',
                'value' => function ($model) {
                    return preg_replace('/\.?0+$/', '', $model->amount); // trim dot&zeros
                },
            ],
            'name',
            'limit',
            'custom_data',
            'created',
            'changed',
        ],
    ]) ?>

</div>
