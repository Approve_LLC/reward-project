<?php

use dosamigos\selectize\SelectizeTextInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-type-form">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#main">Основные</a></li>
        <li><a data-toggle="tab" href="#subTasks">Дочерние задания</a></li>
    </ul>
    <br>
    <div class="tab-content">
        <div id="main" class="tab-pane fade in active">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'application_id')->dropDownList(
                \app\models\Application::getDropdownArr()
            ) ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'min_period')->textInput()->hint('Разрешенное минимальное количество секунд между приемами отчетов о выполнении задания.') ?>

            <?= $form->field($model, 'amount')->textInput() ?>

            <?= $form->field($model, 'limitCount')->textInput([
                'disabled' => true
            ]) ?>

            <?= $form->field($model, 'limit')->textInput() ?>

            <h4>Дополнительные поля:</h4>
            <?php if (!$model->getCustomFieldSet()): ?>
                <p>Дополнительные поля будут доступны после сохранения задания.</p>

                <h4>Выберите набор дополнительных полей:</h4>

                <?= $form->field($model, 'tagNames')->widget(SelectizeTextInput::className(), [
                    'loadUrl' => ['extra-field/list'],
                    'options' => ['class' => 'form-control'],
                    'clientOptions' => [
                        'plugins' => ['remove_button'],
                        'valueField' => 'name',
                        'labelField' => 'name',
                        'searchField' => ['name'],
                        'create' => true,
                    ],
                ])->label(false) ?>
            <?php endif; ?>
            <?php foreach ($model->getCustomTaskTypeFields() as $index => $field): ?>
                <?= $form->field($model, "custom_data[" . $field['name'] . "]")
                    ->label($field['label'])
                    ->{$field['input']}([
                        'value' => $model->getCustomFieldValue($field['name'])
                    ]) ?>
            <?php endforeach; ?>
            <?php foreach ($model->getCustomFieldSet() as $name => $field): ?>
                <?= $form->field($model, "custom_data[$name]")
                    ->label($field['label'])
                    ->{$field['input']}([
                        'value' => $model->getCustomFieldValue($name)
                    ]) ?>
            <?php endforeach; ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div id="subTasks" class="tab-pane fade">
            <?php if ($model->isNewRecord): ?>
                <p>Дочерние задания будут доступны после сохранения задания.</p>
            <?php else: ?>
                <?= $this->render('_subform', [
                    'model' => $model,
                    'subTaskModel' => $subTaskModel,
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
