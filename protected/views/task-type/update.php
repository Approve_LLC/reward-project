<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskType */

$this->title = 'Редактирование типа задания: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типы заданий', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="task-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'subTaskModel' => $subTaskModel
    ]) ?>

</div>
