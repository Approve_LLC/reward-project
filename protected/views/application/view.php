<?php

use app\models\Application;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Приложения', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="application-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'secret',
            'adcolony_key',
            'register_with_promocode_reward',
            'tasks_to_earn_referer_reward',
            'first_referer_reward',
            'next_referer_rewards', [
                'attribute' => 'task_fields_set',
                'value' => Application::getTaskFieldsSetsLabels()[$model->task_fields_set]
            ],
            'created',
            'changed',
        ],
    ]) ?>

</div>
