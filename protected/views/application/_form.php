<?php

use app\models\Application;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secret')->textInput(['maxlength' => true])
            ->hint('Секрет - это пароль, который приложение будет использовать при взаимодействии с бэкэндом.') ?>

    <?= $form->field($model, 'adcolony_key')->textInput(['maxlength' => true])
            ->hint('Ключ с AdColony.com который в '. Html::a('докуметации', 'https://github.com/AdColony/AdColony-Android-SDK-3/wiki/Showing-Rewarded-Interstitial-Ads').' назван MY_SECRET_KEY.') ?>

    <?= $form->field($model, 'register_with_promocode_reward')->textInput(['maxlength' => true])
            ->hint('Начисляется тому кто регистрируется.') ?>

    <?= $form->field($model, 'tasks_to_earn_referer_reward')->textInput(['maxlength' => true])
            ->hint('') ?>

    <?= $form->field($model, 'first_referer_reward')->textInput(['maxlength' => true])
            ->hint('') ?>

    <?= $form->field($model, 'next_referer_rewards')->textInput(['maxlength' => true])
            ->hint('') ?>

    <?= $form->field($model, 'task_fields_set')->dropDownList(Application::getTaskFieldsSetsLabels())
        ->hint('') ?>

    <?= $form->field($model, 'version')->textInput(['type' => 'number'])
        ->hint('') ?>
    <?= $form->field($model, 'force')->checkbox()
        ->hint('') ?>
    <?= $form->field($model, 'update_message')->textarea()
        ->hint('') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
