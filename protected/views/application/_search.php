<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApplicationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'secret') ?>

    <?= $form->field($model, 'adcolony_key') ?>

    <?= $form->field($model, 'created') ?>

    <?php // echo $form->field($model, 'changed') ?>

    <?php // echo $form->field($model, 'register_with_promocode_reward') ?>

    <?php // echo $form->field($model, 'tasks_to_earn_referer_reward') ?>

    <?php // echo $form->field($model, 'first_referer_reward') ?>

    <?php // echo $form->field($model, 'next_referer_rewards') ?>

    <?php // echo $form->field($model, 'task_fields_set') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
