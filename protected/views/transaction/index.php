<?php

use app\models\Transaction;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Транзакции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        Здесь показываются все изменения балансов пользователей, кроме тех что связаны с <?=Html::a('отчетами',['report/index'])?> и <?=Html::a('выплатами',['payment/index'])?>.
    </p>
    <p>
        <?= Html::a('Создать транзакцию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
            'firstPageLabel' => 'First',
            'lastPageLabel'  => 'Last'
        ],
        'columns' => [
            'id',
            'user_id',
            [
                'attribute' => 'type',
                'filter' => Transaction::getTypesLabels(),
                'value' => function ($model, $key, $index, $grid) {
                    return @Transaction::getTypesLabels()[$model->type];
                }
            ],
            'amount',
            //'info:ntext',
            'created',
            //'changed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
