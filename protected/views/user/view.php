<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1>Пользователь #<?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Удалить пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'application_id',
            [
                'attribute' => 'type',
                'value' => User::getTypesLabels()[$model->type],
            ], [
                'attribute' => 'status',
                'value' => User::getStatusLabels()[$model->status],
            ],
            'login',
            [
                'attribute' => 'balance',
                'value' => function ($model) {
                    return preg_replace('/\.?0+$/', '', $model->balance); // trim zeros
                },
            ],
            'promoCode',
            'referrer_user_id',
            [
                'attribute' => 'count_referrers',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->count_referrers, ['user/index', 'UserSearch[referrer_user_id]'=>$model->id]);
                },
            ],
            [
                'attribute' => 'sum_rewards_for_referrers',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->sum_rewards_for_referrers, ['transaction/index', 'TransactionSearch[user_id]'=>$model->id, 'TransactionSearch[type]'=>\app\models\Transaction::TYPE_REFERRAL_REWARD]);
                },
            ],
            'created',
        ],
    ]) ?>

</div>
