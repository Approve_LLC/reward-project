<?php

use app\models\User;
use kartik\export\ExportMenu;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel app\models\UserSearch */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Пересчитать балансы', ['recalculate-balances'], ['class' => 'btn', 'title' => 'На основе отчетов и выплат']) ?>
    </p>

    <?php
    $gridColumns = [
        'id',
        'application_id',
        'login', [
            'attribute' => 'type',
            'filter' => User::getTypesLabels(),
            'value' => function ($model, $key, $index, $grid) {
                return @User::getTypesLabels()[$model->type];
            }
        ],[
            'attribute' => 'status',
            'filter' => User::getStatusLabels(),
            'value' => function ($model, $key, $index, $grid) {
                return @User::getStatusLabels()[$model->status];
            }
        ],
//            'password_hash',
        [
            'attribute' => 'referrer_user_id',
            'headerOptions' => ['title'=>'ID пригласившего пользователя'],
        ],
        [
            'attribute' => 'balance',
            'value' => function ($model, $key, $index, $column) {
                return preg_replace('/\.?0+$/', '', $model->balance); // trim zeros
            },
        ],
        [
            'attribute' => 'count_referrers',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model->count_referrers)
                    return Html::a($model->count_referrers, ['user/index', 'UserSearch[referrer_user_id]'=>$model->id]);
                else
                    return '';
            },
        ],
        [
            'attribute' => 'sum_rewards_for_referrers',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                if ($model->sum_rewards_for_referrers)
                    return Html::a($model->sum_rewards_for_referrers, ['transaction/index', 'TransactionSearch[user_id]'=>$model->id, 'TransactionSearch[type]'=>\app\models\Transaction::TYPE_REFERRAL_REWARD]);
                else
                    return '';
            },
        ],
        //'created',

        ['class' => 'yii\grid\ActionColumn'],
    ];

    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]);

    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]);

    ?>
</div>
