<?php

namespace app\controllers;

use app\models\ExtraField;
use app\models\TaskTypeSearch;
use Yii;
use app\models\TaskType;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\Report;
use app\models\SubTask;
use yii\web\Response;

/**
 * TaskTypeController implements the CRUD actions for TaskType model.
 */
class TaskTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['admin', 'manager'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaskType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TaskType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaskType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskType();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TaskType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $subTaskModel = new SubTask();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'subTaskModel' => $subTaskModel
        ]);
    }

    /**
     * Deletes an existing TaskType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $taskType = $this->findModel($id);

        if (Report::findOne(['task_type_id' => $id]))
            Yii::$app->session->addFlash('error', 'Нельзя удалить тип задания для которого уже есть отчеты.');
        else
            $taskType->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TaskType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddSubtask()
    {
        $post = Yii::$app->request->post('SubTask');
        $id = $post['task_id'];
        $subTaskModel = new SubTask();
        if ($subTaskModel->load(Yii::$app->request->post())) {
            $subTaskModel->setInterval($subTaskModel->extra_field['time_dimension']);
            if ($subTaskModel->save()) {
                $subTaskModel = new SubTask();
            }
        }
        $model = $this->findModel($id);
        return $this->render('_subform', [
            'model' => $model,
            'subTaskModel' => $subTaskModel,
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionUpdateSubtask()
    {
        $post = Yii::$app->request->post('SubTask');
        $taskId = $post['task_id'];
        $id = $post['id'];
        $subTaskModel = SubTask::find()->where(['id' => $id])->one();
        $subTaskModel->scenario = SubTask::SCENARIO_UPDATE;
        $subTaskModel->load(Yii::$app->request->post());
        $subTaskModel->setInterval($subTaskModel->extra_field['time_dimension']);
        $subTaskModel->update();
        $subTaskModel->interval = $this->getInterval();
        $model = $this->findModel($taskId);
        return $this->render('_subform', [
            'model' => $model,
            'subTaskModel' => new SubTask(),
        ]);
    }

    /**
     * @return null
     */
    public function actionDeleteSubtask()
    {
        $get = Yii::$app->request->get();
        $id = $get['id'];
        $taskId = $get['taskId'];
        SubTask::deleteAll(['id' => $id]);
        $this->redirect('update?id=' . $taskId);
    }

}
