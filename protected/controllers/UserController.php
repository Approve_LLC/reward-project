<?php

namespace app\controllers;

use Yii;
use yii\db\IntegrityException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Transaction;
use app\models\UserSearch;
use app\models\User;
use app\models\Report;
use app\models\Payment;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'roles' => ['admin'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $auth = Yii::$app->authManager;
            $type = $model->type;
            switch ($type) {
                case User::TYPE_USER :
                    $user = $auth->getRole('user');
                    $auth->assign($user, $model->getId());
                    break;
                case User::TYPE_MANAGER :
                    $manager = $auth->getRole('manager');
                    $auth->assign($manager, $model->getId());
                    break;
                case User::TYPE_ADMIN :
                    $admin = $auth->getRole('admin');
                    $auth->assign($admin, $model->getId());
                    break;
            }
            
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
	    Report::deleteAll('user_id = :user_id', [':user_id' => $id]);
	    Payment::deleteAll('user_id = :user_id', [':user_id' => $id]);
	    Transaction::deleteAll('user_id = :user_id', [':user_id' => $id]);
            $this->findModel($id)->delete();
        } catch (IntegrityException $e) {
            Yii::$app->session->addFlash('error', 'Ошибка: '.$e);
        }

        return $this->redirect(['index']);
    }

    public function actionRecalculateBalances()
    {
        $affected = User::recalculateBalances();
        return $this->render('@app/views/site/message', [
            'message'=>"Балансы пользователей пересчитаны.\nЗатронуто пользователей: $affected."
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        /* @var User $model */
        $model = User::find()
            ->select('u.*,
                count(r.id) AS count_referrers,
                sum(t.amount) * count(distinct t.id) / count(t.id) AS sum_rewards_for_referrers')
            ->alias('u')
            ->leftJoin(User::tableName().' r', 'u.id = r.referrer_user_id')
            ->leftJoin(Transaction::tableName().' t', 'u.id = t.user_id AND t.type = :type')
            ->addParams([':type'=>Transaction::TYPE_REFERRAL_REWARD])
            ->groupBy('u.id')
            ->where(['u.id' => $id])
            ->one();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
