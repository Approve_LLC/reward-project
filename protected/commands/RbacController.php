<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;
use yii\console\Exception;
use yii\db\IntegrityException;
use yii\helpers\Console;

/**
 * Class RbacController
 * @package app\commands
 */
class RbacController extends Controller
{
    /**
     * @throws \Exception
     */
    public function actionInit()
    {
        try {
            $auth = Yii::$app->authManager;
            $user = $auth->createRole('user');
            $auth->add($user);
            $manager = $auth->createRole('manager');
            $auth->add($manager);
            $admin = $auth->createRole('admin');
            $auth->add($admin);
            $users = User::find()->all();
            foreach ($users as $userObj) {
                $type = $userObj->type;
                switch ($type) {
                    case User::TYPE_USER :
                        $auth->assign($user, $userObj->id);
                        break;
                    case User::TYPE_MANAGER :
                        $auth->assign($manager, $userObj->id);
                        break;
                    case User::TYPE_ADMIN :
                        $auth->assign($admin, $userObj->id);
                        break;
                }
            }
        } catch (IntegrityException $e) {
            $message = $e->getMessage();
            $this->stdout("Ошибка базы данных.\n", Console::FG_RED);
            $this->stdout("$message\n", Console::FG_RED);
        }
    }

    /**
     * @throws \Exception
     */
    public function actionUpdate()
    {
        try {
            $auth = Yii::$app->authManager;
            $user = $auth->getRole('user');
            $manager = $auth->getRole('manager');
            $admin = $auth->getRole('admin');
            $users = User::find()->all();
            foreach ($users as $userObj) {
                $type = $userObj->type;
                switch ($type) {
                    case User::TYPE_USER :
                        $auth->revoke($user, $userObj->id);
                        $auth->assign($user, $userObj->id);
                        break;
                    case User::TYPE_MANAGER :
                        $auth->revoke($manager, $userObj->id);
                        $auth->assign($manager, $userObj->id);
                        break;
                    case User::TYPE_ADMIN :
                        $auth->revoke($admin, $userObj->id);
                        $auth->assign($admin, $userObj->id);
                        break;
                }
            }
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->stdout("Ошибка базы данных.\n", Console::FG_RED);
            $this->stdout("$message\n", Console::FG_RED);
        }
    }
}
