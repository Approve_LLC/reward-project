<?php

namespace app\commands;

use yii\console\Controller;

use app\models\TaskType;

/**
 * Class RefreshController
 * @package app\commands
 */
class RefreshController extends Controller
{

    public function actionIndex()
    {
        TaskType::updateAll(['limitCount' => 0]);
    }
}