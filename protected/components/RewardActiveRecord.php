<?php

namespace app\components;

use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class RewardActiveRecord extends ActiveRecord
{
    public function behaviors()
    {
        return [
            'timestamp'=> [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'changed',
                'value' => new Expression('NOW()'),
                'attributes' => [
                    BaseActiveRecord::EVENT_BEFORE_UPDATE => 'changed',
                ],
            ],
        ];
    }
}