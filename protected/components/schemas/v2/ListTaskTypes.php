<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class ListTaskTypes
{
    /**
     * Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $id;

    /**
     * Name
     * @var string
     * @OA\Property()
     */
    public $name;

    /**
     * Reward for task
     * @var int
     * @OA\Property(format="int32")
     */
    public $reward;

    /**
     * Period to get task again.
     * @var int
     * @OA\Property(format="int32")
     */
    public $wait;

    /**
     * Subtasks count
     * @var string
     * @OA\Property()
     */
    public $custom_data;

    /**
     * User Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $subtasks_total;

    /**
     * Closed subtasks count
     * @var int
     * @OA\Property(format="int32")
     */
    public $closed_subtasks;

    /**
     * Subtasks flag
     * @var bool
     * @OA\Property(format="boolean")
     */
    public $hasSubtasks;

    /**
     * Current task are closed?
     * @var bool
     * @OA\Property(format="boolean")
     */
    public $taskDone;
}
