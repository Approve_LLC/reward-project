<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class Application
{
    /**
     * Version
     * @var string
     * @OA\Property()
     */
    public $latestVersion;

    /**
     * Force upgrade
     * @var boolean
     * @OA\Property()
     */
    public $forceUpgrade;

    /**
     * Version
     * @var string
     * @OA\Property()
     */
    public $updateMessage;
}