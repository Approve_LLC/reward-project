<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class Login
{
    /**
     * User Status
     * @var int
     * @OA\Property(format="int32")
     */
    public $status;

    /**
     * Session Id
     * @var string
     * @OA\Property()
     */
    public $session_id;

    /**
     * User Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $user_id;
}
