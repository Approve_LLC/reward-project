<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema(required={"task_type_id", "date_start", "date_start", "date_start", "date_start"})
 */
class SaveReportForm
{
    /**
     * Type task ID
     * @var int
     * @OA\Property(format="int32")
     */
    public $task_type_id;

    /**
     * Task start time. Zero timezone. Format: 2018-02-20T01:59:04
     * @var string
     * @OA\Property()
     */
    public $date_start;

    /**
     * Task end time. Zero timezone. Format: 2018-02-20T01:59:04
     * @var string
     * @OA\Property()
     */
    public $date_end;

    /**
     * md5 hash get by string is made from
     * 3 params and password(1234SecRet777xG) in order:
     * task_type_id+date_start+date_end+1234SecRet777xG
     * md5 хэш от строки, полученной конкатенацией всех 3х параметров в указанном выше порядке и пароля - "
     * @var string
     * @OA\Property()
     */
    public $hash;

    /**
     * Subtask ID
     * @var int
     * @OA\Property(format="int32")
     */
    public $subtask_id;
}