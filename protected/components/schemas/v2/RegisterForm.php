<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema(required={"login", "password", "app_id"})
 */
class RegisterForm
{
    /**
     * User login.
     * @var string
     * @OA\Property(format="")
     */
    public $login;

    /**
     * Password
     * @var string
     * @OA\Property()
     */
    public $password;

    /**
     * Application id
     * @var int
     * @OA\Property(format="int32")
     */
    public $app_id;

    /**
     * User's promocode who's invite you
     * @var string
     * @OA\Property()
     */
    public $referer_promocode;
}