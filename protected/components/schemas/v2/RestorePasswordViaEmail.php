<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class RestorePasswordViaEmail
{
    /**
     * Status
     * @var int
     * @OA\Property(format="int32")
     */
    public $status;

    /**
     * User Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $user_id;
}
