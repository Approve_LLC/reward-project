<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class Subtasks
{
    /**
     * Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $subtask_id;

    /**
     * Task Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $task_id;

    /**
     * Json filled with custom data
     * @var object
     * @OA\Property(
     *     format="",
     *     additionalProperties=true
     * )
     */
    public $custom_data;

    /**
     * Reward for subtask
     * @var int
     * @OA\Property(format="int32")
     */
    public $amount;

    /**
     * One of subtask's types
     * @var int
     * @OA\Property(format="int32")
     */
    public $type;

    /**
     * Interval which next subtask can available again
     * @var int
     * @OA\Property(format="int32")
     */
    public $interval;

    /**
     * Depending on the subtask's type
     * @var object
     * @OA\Property(
     *     format="",
     *     additionalProperties=true
     * )
     */
    public $extra_field;

    /**
     * Subtask are closed?
     * @var int
     * @OA\Property(format="int32")
     */
    public $closed;
}