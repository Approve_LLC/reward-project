<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema(required={"task_type_id"})
 */
class DeleteReportsForm
{
    /**
     * Task type id
     * @var int
     * @OA\Property(format="int32")
     */
    public $task_type_id;
}