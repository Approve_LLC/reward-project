<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class User
{
    /**
     * Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $id;

    /**
     * Balance
     * @var float
     * @OA\Property()
     */
    public $balance;

    /**
     * Promo code
     * @var string
     * @OA\Property()
     */
    public $promoCode;

    /**
     * Count referrers
     * @var int
     * @OA\Property(format="int32")
     */
    public $countReferrers;

    /**
     * All referrers' sum
     * @var float
     * @OA\Property()
     */
    public $sumRewardsForReferrers;
}