<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema(required={"login", "app_id", "code", "new_password"})
 */
class RestorePasswordForm
{
    /**
     * User login.
     * @var string
     * @OA\Property(format="")
     */
    public $login;

    /**
     * Restore code from email.
     * @var string
     * @OA\Property(format="")
     */
    public $code;

    /**
     * User new password.
     * @var string
     * @OA\Property(format="")
     */
    public $new_password;

    /**
     * Application id
     * @var int
     * @OA\Property(format="int32")
     */
    public $app_id;
}