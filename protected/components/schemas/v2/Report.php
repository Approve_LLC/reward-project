<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema()
 */
class Report
{
    /**
     * Status
     * @var int
     * @OA\Property(format="int32")
     */
    public $status;

    /**
     * Response message
     * @var string
     * @OA\Property()
     */
    public $message;

    /**
     * Amount
     * @var float
     * @OA\Property()
     */
    public $amount;
}