<?php

namespace app\components\schemas\v2;

/**
 * @OA\Schema(required={"email", "app_id"})
 */
class RestorePasswordViaEmailForm
{
    /**
     * User email.
     * @var string
     * @OA\Property(format="")
     */
    public $email;

    /**
     * Application id
     * @var int
     * @OA\Property(format="int32")
     */
    public $app_id;
}