<?php

namespace app\components\schemas\v1;

/**
 * @OA\Schema(required={"promocode"})
 */
class SetPromocodeForm
{
    /**
     * Promocode.
     * @var string
     * @OA\Property()
     */
    public $promocode;
}