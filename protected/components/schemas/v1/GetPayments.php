<?php

namespace app\components\schemas\v1;

/**
 * @OA\Schema()
 */
class GetPayments
{
    /**
     * Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $id;

    /**
     * Response message
     * @var string
     * @OA\Property(
     *     enum={"waiting", "executing", "done"}
     * )
     */
    public $status;

    /**
     * Payment amount
     * @var int
     * @OA\Property(format="int32")
     */
    public $amount;

    /**
     * Payment type.
     * @var string
     * @OA\Property()
     */
    public $payment_type;

    /**
     * Payment account.
     * @var string
     * @OA\Property()
     */
    public $payment_account;
}