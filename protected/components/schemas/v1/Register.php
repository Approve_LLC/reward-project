<?php

namespace app\components\schemas\v1;

/**
 * @OA\Schema()
 */
class Register
{
    /**
     * User Status
     * @var int
     * @OA\Property(format="int32")
     */
    public $status;

    /**
     * Response message
     * @var string
     * @OA\Property()
     */
    public $message;

    /**
     * User Id
     * @var int
     * @OA\Property(format="int32")
     */
    public $user_id;
}
