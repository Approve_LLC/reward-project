<?php

namespace app\components\schemas\v1;

/**
 * @OA\Schema()
 */
class SaveReport
{
    /**
     * Status
     * @var int
     * @OA\Property(format="int32")
     */
    public $status;

    /**
     * Response message
     * @var string
     * @OA\Property()
     */
    public $message;
}