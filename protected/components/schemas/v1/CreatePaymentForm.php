<?php

namespace app\components\schemas\v1;

/**
 * @OA\Schema(required={"amount", "payment_type", "payment_account"})
 */
class CreatePaymentForm
{
    /**
     * Sum for pay
     * @var int
     * @OA\Property(format="int32")
     */
    public $amount;

    /**
     * Pay system identification string example: 'paypal','webmoney','qiwi','phone'.
     * @var string
     * @OA\Property()
     */
    public $payment_type;

    /**
     * Bill identification string in pay system(номер кошелька или телефона, на который надо вывести оплату).
     * @var string
     * @OA\Property()
     */
    public $payment_account;
}
