<?php

namespace tests\models;

use app\models\Report;
use app\models\User;
use Yii;

class ReportTest extends \Codeception\Test\Unit
{
    /**
     * @group report
     */
    public function testSaveReport()
    {
        $userId = 2;
        $taskId = 1;

        expect_that($user = User::findIdentity($userId));

        $startBalance = $user->balance;

        // save report
        $report = new Report(['scenario'=>'api']);
        $report->task_type_id = $taskId;
        $report->hash = md5($taskId . $user->getApplication()->secret);
        $report->setAmountFromTaskType();
        $report->setUser($user);
        $secondsToWait = $report->getTaskType()->min_period - $report->getSecondsFromLastReport();
        if ($secondsToWait > 0)
            sleep($secondsToWait + 1);
        $isSaved = $report->save();
        expect_that($isSaved);

        // check balance
        $user = User::findIdentity($userId);
        expect($user->balance)->equals($startBalance + $report->amount);
    }

    /**
     * @group report
     */
    public function testReport()
    {
        $report = new Report(['scenario'=>'api']);
        expect_that($report->isAttributeRequired('amount'));
    }
}
