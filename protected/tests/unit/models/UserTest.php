<?php

namespace tests\models;

use app\models\User;
use Codeception\Test\Unit;

class UserTest extends Unit
{
    /**
     * @group user
     */
    public function testFindById()
    {
        expect_that($user = User::findIdentity(1));
        expect($user->login)->equals('admin@admin.ru');

        expect_not(User::findIdentity(999));
    }

    /**
     * @group user
     */
    public function testFindByLogin()
    {
        expect_that($user = User::findByLogin('admin@admin.ru'));
        expect_not(User::findByLogin('not-exist'));
    }

    /**
     * @depends testFindByLogin
     * @group user
     */
    public function testValidatePassword()
    {
        $user = User::findByLogin('admin@admin.ru');
        expect_that($user->validatePassword('123'));
        expect_not($user->validatePassword('wrong password'));
    }

    /**
     * @group promocode
     */
    public function testFindByPromoCode()
    {
        expect_that($user = User::findIdentity(2));
        $user->promoCode = 'asd';
        expect_that($user->save());

        expect_that($user = User::findByPromoCode('asd'));
        expect_that($user->promoCode == 'asd');
    }

}
