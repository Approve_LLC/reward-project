<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'reward',
    'name' => 'reward',
    'language' => 'ru',
    'sourceLanguage' => 'ru',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'app\modules\v1\Module'
        ],
        'v2' => [
            'basePath' => '@app/modules/v2',
            'class' => 'app\modules\v2\Module'
        ],
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ]
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'DfVemaTR8TKlWUz2hsv6u2sk8aNUGAaI',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'log' => [
            'traceLevel' => 0, //YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'maxFileSize' => 100,
                    'maxLogFiles' => 30,
                    'categories' => ['api'],
                    'logVars' => [
                        '_GET',
                        '_POST',
                        '_SERVER.REQUEST_METHOD',
                        '_SERVER.REMOTE_ADDR',
                        '_SERVER.HTTP_USER_AGENT',
                    ],
                    'logFile' => '@runtime/logs/api.log',
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'maxFileSize' => 100,
                    'maxLogFiles' => 50,
                    'categories' => ['api_response'],
                    'logVars' => [],
                    'logFile' => '@runtime/logs/api_response.log',
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                '<module:v1>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<controller:(application|extra-field|payment|report|site|task-type|transaction|user)>/<action>' => '<controller>/<action>',
                '<module:v2>/<controller:auth>/<action>' => '<module>/<controller>/<action>',
                'GET <module:v2>/<controller:task-type>s/<taskId:\d+>/subtasks' => '<module>/<controller>/get-subtasks',
                'DELETE <module:v2>/<controller:task-type>s/<taskTypeId:\d+>/reports' => '<module>/<controller>/delete-reports',
                'GET <module:v2>/<controller:user>/get-time' => '<module>/<controller>/get-time',
                'POST <module:v2>/<controller:user>/set-promocode' => '<module>/<controller>/set-promocode',
                '/' => '/',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v2/payment'
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v2/report'
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v2/task-type',
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v2/application',
                    'only' => ['view']
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => [
                        'v2/user' => 'v2/user'
                    ],
                    'patterns' => [
                        'GET /' => 'view',
                    ],
                ]
            ],
        ],
    ],
    'params' => $params,
];

if (10) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
