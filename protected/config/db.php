<?php

$db = require __DIR__ . '/db-local.php';

return array_merge([
    'class' => 'yii\db\Connection',
    'charset' => 'utf8',
//    'dsn' => 'mysql:host=localhost;dbname=reward',
//    'username' => 'root',
//    'password' => 'q',
    'dsn' => 'mysql:host=localhost;dbname=ostap88366_rewrd',
    'username' => 'root',
    'password' => '',
    // Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
], $db);
