<?php

$params = require __DIR__ . '/params-local.php';

return array_merge([
    'adminEmail' => 'admin@example.com',
    'appodealEncryptionKey' => 'zzzzzz07a1ca68efa946f78b4f05e6e048',
    'chartboostS2Skey' => 'xxxxDAFEG34532xhfhrt45xx'
], $params);